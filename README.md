# Lobster Roll
## A 2D physics arcade game
Spiders are invading the ocean, and you are the first and only line of defense. Thanks to your sharp lobster claws, you alone can hit the spiders where they're vulnerable—their webs. You can count on help from a few of your undersea friends, but beware—some of your fellow sea creatures have turned traitor.

Slice spiders' webs to defeat them, but don't let the spiders hit you. Get powerups to get an edge, and watch out for crafty hermit crabs. Beat your high score by killing more spiders and getting better combos.

Download here: https://jfletcher94.itch.io/lobster-roll