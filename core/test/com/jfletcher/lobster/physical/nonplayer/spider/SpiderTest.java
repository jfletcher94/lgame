package com.jfletcher.lobster.physical.nonplayer.spider;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.MockitoAnnotations.initMocks;

import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;

class SpiderTest {

  private static final float TIMESTEP = 0.1f;
  private static final int VELOCITY_ITERATIONS = 1;
  private static final int POSITION_ITERATIONS = 1;

  @Mock
  private SpriteLoader spriteLoader;
  @Mock
  private Level level;
  @Mock
  private Texture texture;
  @Mock
  private Body body;
  @Mock
  private Web web;

  private Vector2 position;
  private Vector2 velocity;
  private Sprite sprite;

  private Spider spider;

  @BeforeEach
  void setUp() {
    initMocks(this);
    position = Vector2.Zero;
    velocity = Vector2.Zero;
    sprite = new Sprite(texture);

    spider = spy(new Spider());
    doReturn(spider).when(spider).createSpiderSprite();
    doReturn(body).when(spider).createBody(any(), any());
    doReturn(web).when(spider).createWeb();
  }
}
