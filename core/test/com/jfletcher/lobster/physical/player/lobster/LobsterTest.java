package com.jfletcher.lobster.physical.player.lobster;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.contact.LobsterContact;
import com.jfletcher.lobster.physical.environment.friendly.powerup.PowerUp;
import com.jfletcher.lobster.physical.nonplayer.spider.Spider;
import com.jfletcher.lobster.physical.nonplayer.spider.Web;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class LobsterTest {

  private static final float TIMESTEP = 0.1f;
  private static final int VELOCITY_ITERATIONS = 1;
  private static final int POSITION_ITERATIONS = 1;

  @Mock
  private SpriteLoader spriteLoader;
  @Mock
  private Level level;
  @Mock
  private Texture texture;
  @Mock
  private Body body;

  private Vector2 position;
  private Vector2 velocity;
  private Sprite sprite;

  private Lobster lobster;

  @BeforeEach
  void setUp() {
    initMocks(this);
    position = Vector2.Zero;
    velocity = Vector2.Zero;
    sprite = new Sprite(texture);

    lobster = spy(new Lobster());
    doReturn(sprite).when(lobster).createAnimationState();
    doReturn(body).when(lobster).createMovementState(any(), any());
    lobster.create(spriteLoader, level, Vector2.Zero, Vector2.Zero);
  }

  @Nested
  class TestStep {

    @Test
    void noChanges() {
      when(body.getLinearVelocity()).thenReturn(Vector2.Zero);
      when(body.getPosition()).thenReturn(Vector2.Zero);

      lobster.step(TIMESTEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);

      verify(body, never()).applyForceToCenter(any(), anyBoolean());
      verify(body, never()).setLinearVelocity(anyFloat(), anyFloat());
    }

    @Test
    void overSpeedPositive() {
      when(body.getPosition()).thenReturn(Vector2.Zero);
      when(body.getLinearVelocity()).thenReturn(new Vector2(1.0e10f, 0.0f));

      doAnswer(
          invocation -> {
            assertTrue((float) invocation.getArgument(0) < 1.0e10f);
            return null;
          })
          .when(body)
          .setLinearVelocity(anyFloat(), anyFloat());

      lobster.step(TIMESTEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);

      verify(body, never()).applyForceToCenter(any(), anyBoolean());
    }

    @Test
    void overSpeedNegative() {
      when(body.getPosition()).thenReturn(Vector2.Zero);
      when(body.getLinearVelocity()).thenReturn(new Vector2(-1.0e10f, 0.0f));

      doAnswer(
          invocation -> {
            assertTrue((float) invocation.getArgument(0) > -1.0e10f);
            return null;
          })
          .when(body)
          .setLinearVelocity(anyFloat(), anyFloat());

      lobster.step(TIMESTEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);

      verify(body, never()).applyForceToCenter(any(), anyBoolean());
    }

    @Test
    void speedDeath() {
      when(body.getPosition()).thenReturn(new Vector2(0.0f, -1_000.0f));
      when(body.getLinearVelocity()).thenReturn(new Vector2(0.0f, 1.0e-10f));

      lobster.step(TIMESTEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);

      verify(body, never()).applyForceToCenter(any(), anyBoolean());
      verify(body, never()).setLinearVelocity(anyFloat(), anyFloat());
    }
  }

  @Test
  void testDestroy() {
    assertFalse(lobster.isDestroyed());
    lobster.destroy();
    assertTrue(lobster.isDestroyed());
    lobster.destroy();
    assertTrue(lobster.isDestroyed());

    verify(level).destroy(lobster);
  }

  @Nested
  class TestContact {

    @Mock
    private LobsterContact lobsterContact;

    @BeforeEach
    void setUp() {
      initMocks(this);
      when(lobsterContact.getLobster()).thenReturn(lobster);
    }

    @Test
    void contactWeb() {
      when(lobsterContact.getContactable()).thenReturn(mock(Web.class));
      lobsterContact.apply();
      verify(lobster).contactWeb(any(), any());
    }

    @Test
    void contactSpider() {
      when(lobsterContact.getContactable()).thenReturn(mock(Spider.class));
      lobsterContact.apply();
      verify(lobster).contactSpider(any(), any());
    }

    @Test
    void contactPowerUp() {
      when(lobsterContact.getContactable()).thenReturn(mock(PowerUp.class));
      lobsterContact.apply();
      verify(lobster).contactPowerUp(any(), any());
    }
  }

  @Nested
  class TestContactWeb {

    @Mock
    private LobsterContact lobsterContact;
    @Mock
    private Contact contact;
    @Mock
    private Web web;

    private Vector2 velocity;

    @BeforeEach
    void setUp() {
      initMocks(this);
      velocity = Vector2.Zero.cpy();

      when(lobsterContact.getContact()).thenReturn(contact);
      when(lobsterContact.getContactable()).thenReturn(web);
      when(body.getLinearVelocity()).thenReturn(velocity);
    }

    @Test
    void testHighPositiveSpeed() {
      velocity.set(0.0f, 1_000.0f);

      doAnswer(
          invocation -> {
            assertTrue((float) invocation.getArgument(1) > velocity.y);
            return null;
          })
          .when(body)
          .setLinearVelocity(anyFloat(), anyFloat());

      lobster.contactWeb(lobsterContact, web);
    }

    @Test
    void testMiddleSpeed() {
      velocity.set(0.0f, 0.0f);

      doAnswer(
          invocation -> {
            assertTrue((float) invocation.getArgument(1) > velocity.y);
            return null;
          })
          .when(body)
          .setLinearVelocity(anyFloat(), anyFloat());

      lobster.contactWeb(lobsterContact, web);
    }

    @Test
    void testHighNegativeSpeed() {
      velocity.set(0.0f, -1_000.0f);

      doAnswer(
          invocation -> {
            assertEquals((float) invocation.getArgument(1), -velocity.y);
            return null;
          })
          .when(body)
          .setLinearVelocity(anyFloat(), anyFloat());

      lobster.contactWeb(lobsterContact, web);
    }
  }
}
