package com.jfletcher.lobster.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.google.common.base.Strings;

import java.util.List;

public class PreferenceManager {

  private static final int SCORES_TO_KEEP = 5;

  private static final String HIGH_SCORE = "LGame High Score";
  private static final String BEST_KILL = "LGame Best Kill";
  private static final String KILL_COUNT = "LGame Kill Count";

  private static final Preferences highScore = Gdx.app.getPreferences(HIGH_SCORE);
  private static final Preferences bestKill = Gdx.app.getPreferences(BEST_KILL);
  private static final Preferences killCount = Gdx.app.getPreferences(KILL_COUNT);

  public static void init() {
    assert HighScore.HIGH_SCORE_DEFAULTS.size() >= SCORES_TO_KEEP;
    assert HighScore.BEST_KILL_DEFAULTS.size() >= SCORES_TO_KEEP;
    assert HighScore.KILL_COUNT_DEFAULTS.size() >= SCORES_TO_KEEP;
    initHelper(highScore, HighScore.HIGH_SCORE_DEFAULTS);
    initHelper(bestKill, HighScore.BEST_KILL_DEFAULTS);
    initHelper(killCount, HighScore.KILL_COUNT_DEFAULTS);
  }

  public static HighScore[] getHighScores() {
    return getHelper(highScore);
  }

  public static HighScore[] getBestKills() {
    return getHelper(bestKill);
  }

  public static HighScore[] getKillCounts() {
    return getHelper(killCount);
  }

  public static int putHighScore(final long score, final String name) {
    return putHelper(score, name, highScore);
  }

  public static int putBestKill(final long score, final String name) {
    return putHelper(score, name, bestKill);
  }

  public static int putKillCount(final int score, final String name) {
    return putHelper(score, name, killCount);
  }

  public static void renameHighScore(final int i, final String name) {
    renameHelper(i, name, highScore);
  }

  public static void renameBestKill(final int i, final String name) {
    renameHelper(i, name, bestKill);
  }

  public static void renameKillCount(final int i, final String name) {
    renameHelper(i, name, killCount);
  }

  private static int putHelper(final long value, final String name, final Preferences preferences) {
    final HighScore[] highScores = getHelper(preferences);
    int index = 0;
    for (; index < SCORES_TO_KEEP; index++) {
      if (value > highScores[index].getValue()) {
        break;
      }
    }
    if (index == SCORES_TO_KEEP) {
      return -1;
    }

    HighScore nextHighScore = new HighScore(value, name);
    for (int i = index; i < SCORES_TO_KEEP; i++) {
      preferences.putString(String.valueOf(i), nextHighScore.toString());
      nextHighScore = highScores[i];
    }
    preferences.flush();
    return index;
  }

  private static void renameHelper(final int i, final String name, final Preferences preferences) {
    final HighScore highScore = getHelper(preferences)[i];
    highScore.setName(name);
    preferences.putString(String.valueOf(i), highScore.toString());
    preferences.flush();
  }

  private static HighScore[] getHelper(final Preferences preferences) {
    final HighScore[] highScores = new HighScore[SCORES_TO_KEEP];
    for (int i = 0; i < SCORES_TO_KEEP; i++) {
      highScores[i] = HighScore.fromString(preferences.getString(String.valueOf(i)));
    }
    return highScores;
  }

  private static void initHelper(final Preferences preferences, final List<HighScore> defaults) {
    boolean changed = false;
    for (int i = 0; i < SCORES_TO_KEEP; i++) {
      final String s = String.valueOf(i);
      if (Strings.isNullOrEmpty(preferences.getString(s, ""))) {
        preferences.putString(s, defaults.get(i).toString());
        changed = true;
      }
    }
    if (changed) {
      preferences.flush();
    }
  }
}
