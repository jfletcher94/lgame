package com.jfletcher.lobster.hud;

import com.google.common.collect.ImmutableList;
import com.jfletcher.lobster.physical.player.lobster.Score;

import java.text.ParseException;
import java.util.regex.Pattern;

public class HighScore {

  public static final ImmutableList<HighScore> HIGH_SCORE_DEFAULTS =
      ImmutableList.of(
          new HighScore(1_000_000, "Homarus"),
          new HighScore(100_000, "Metanephrops"),
          new HighScore(10_000, "Thaumastocheles"),
          new HighScore(1_000, "Eunephrops"),
          new HighScore(100, "Dinochelus"));
  public static final ImmutableList<HighScore> KILL_COUNT_DEFAULTS =
      ImmutableList.of(
          new HighScore(100, "Homarus"),
          new HighScore(75, "Metanephrops"),
          new HighScore(50, "Thaumastocheles"),
          new HighScore(25, "Eunephrops"),
          new HighScore(10, "Dinochelus"));
  public static final ImmutableList<HighScore> BEST_KILL_DEFAULTS =
      ImmutableList.of(
          new HighScore(100_000, "Homarus"),
          new HighScore(10_000, "Metanephrops"),
          new HighScore(1_000, "Thaumastocheles"),
          new HighScore(100, "Eunephrops"),
          new HighScore(10, "Dinochelus"));

  private static final String VALUE_SEPARATOR = " ";
  private static final String VALUE_FORMAT = "%d" + VALUE_SEPARATOR + "%s";

  private long value;
  private String name;

  public HighScore(final long value, final String name) {
    this.name = name;
    this.value = value;
  }

  public long getValue() {
    return value;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return String.format(VALUE_FORMAT, getValue(), getName());
  }

  public static HighScore fromString(final String s) {
    final var scoreData = s.split(Pattern.quote(HighScore.VALUE_SEPARATOR), 2);
    try {
      return new HighScore(Score.decimalFormat.parse(scoreData[0]).longValue(), scoreData[1]);
    } catch (final ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public void setName(final String name) {
    this.name = name;
  }
}
