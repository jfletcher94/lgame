package com.jfletcher.lobster.hud;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.jfletcher.lobster.physical.player.lobster.Score;
import com.jfletcher.lobster.visual.StyleManager;

import java.util.HashMap;
import java.util.Map;

public class ScoreMenu {

  private static final Label.LabelStyle titleLabelStyle =
      StyleManager.createLabelStyle(32, Color.BLACK, 4, Color.WHITE);
  private static final Label.LabelStyle headerLabelStyle =
      StyleManager.createLabelStyle(24, Color.BLACK, 3, Color.WHITE);
  private static final Label.LabelStyle textLabelStyle =
      StyleManager.createLabelStyle(16, Color.BLACK, 2, Color.WHITE);
  private static final Label.LabelStyle newLabelStyle =
      StyleManager.createLabelStyle(16, Color.RED, 2, Color.WHITE);

  private final Table table;
  private final Map<Integer, Cell<Label>> newCellMap = new HashMap<>();
  private final Map<Integer, Label> updateScoreMap = new HashMap<>();
  private final Map<Integer, Label> updateNameMap = new HashMap<>();

  public ScoreMenu() {
    final HighScore[] highScores = PreferenceManager.getHighScores();
    final HighScore[] bestKills = PreferenceManager.getBestKills();
    final HighScore[] killCounts = PreferenceManager.getKillCounts();

    table = new Table();
    table.add(new Label("High Scores", titleLabelStyle)).center().colspan(9).padBottom(24f);
    table.row().padBottom(12f);

    table.add(new Label("Best Kill", headerLabelStyle)).center().colspan(3);
    table.add(new Label("Score", headerLabelStyle)).center().colspan(3);
    table.add(new Label("Kills", headerLabelStyle)).center().colspan(3);
    table.row().padBottom(12f);
    for (int i = 0; i < highScores.length; i++) {
      newCellMap.put(i, table.add(new Label("", newLabelStyle)).minWidth(48f));
      updateScoreMap.put(
          i,
          table
              .add(new Label(Score.decimalFormat.format(bestKills[i].getValue()), textLabelStyle))
              .right()
              .padRight(8f)
              .getActor());
      updateNameMap.put(
          i,
          table
              .add(new Label(bestKills[i].getName(), textLabelStyle))
              .right()
              .padRight(8f)
              .getActor());

      newCellMap.put(i + 5, table.add(new Label("", newLabelStyle)).minWidth(48f));
      updateScoreMap.put(
          i + 5,
          table
              .add(new Label(Score.decimalFormat.format(highScores[i].getValue()), textLabelStyle))
              .right()
              .padRight(8f)
              .getActor());
      updateNameMap.put(
          i + 5,
          table
              .add(new Label(highScores[i].getName(), textLabelStyle))
              .right()
              .padRight(8f)
              .getActor());

      newCellMap.put(i + 10, table.add(new Label("", newLabelStyle)).minWidth(48f));
      updateScoreMap.put(
          i + 10,
          table
              .add(new Label(Score.decimalFormat.format(killCounts[i].getValue()), textLabelStyle))
              .right()
              .padRight(8f)
              .getActor());
      updateNameMap.put(
          i + 10,
          table
              .add(new Label(killCounts[i].getName(), textLabelStyle))
              .right()
              .padRight(8f)
              .getActor());
      table.row().padBottom(12f);
    }
  }

  public void setNew(final int newBestKill, final int newScore, final int newKills) {
    if (newBestKill >= 0) {
      newCellMap.get(newBestKill).padRight(8f).padLeft(12f).getActor().setText("New!");
      updateScoreMap.get(newBestKill).setStyle(newLabelStyle);
      updateNameMap.get(newBestKill).setStyle(newLabelStyle);
    }
    if (newScore >= 0) {
      newCellMap.get(newScore + 5).padRight(8f).padLeft(12f).getActor().setText("New!");
      updateScoreMap.get(newScore + 5).setStyle(newLabelStyle);
      updateNameMap.get(newScore + 5).setStyle(newLabelStyle);
    }
    if (newKills >= 0) {
      newCellMap.get(newKills + 10).padRight(8f).padLeft(12f).getActor().setText("New!");
      updateScoreMap.get(newKills + 10).setStyle(newLabelStyle);
      updateNameMap.get(newKills + 10).setStyle(newLabelStyle);
    }
    table.pack();
  }

  public Table getTable() {
    return table;
  }
}
