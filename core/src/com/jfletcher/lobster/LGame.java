package com.jfletcher.lobster;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.jfletcher.lobster.hud.PreferenceManager;
import com.jfletcher.lobster.level.FreePlay;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.level.Splash;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.InputManager;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

public class LGame extends Game {

  private SpriteLoader spriteLoader;
  private InputManager inputManager;
  private Splash splash;
  private Level level;
  private boolean start = false;

  @Override
  public void create() {
    Box2D.init();
    PreferenceManager.init();
    spriteLoader = new SpriteLoader();
    inputManager = new InputManager();
    Gdx.input.setInputProcessor(inputManager);
    createSplash();
  }

  @Override
  public void dispose() {
    level.dispose();
    super.dispose();
  }

  @Override
  public void render() {
    super.render();
    inputManager.step(
        Constants.Step.TIME_STEP,
        Constants.Step.VELOCITY_ITERATIONS,
        Constants.Step.POSITION_ITERATIONS);

    if (!start) {
      splash.step(
          Constants.Step.TIME_STEP,
          Constants.Step.VELOCITY_ITERATIONS,
          Constants.Step.POSITION_ITERATIONS);
      splash.render();
      if (splash.isStart()) {
        start = true;
        splash.dispose();
        createLevel();
      }
      return;
    }

    if (level.toRestart()) {
      level.dispose();
      createLevel();
    }

    level.step(
        Constants.Step.TIME_STEP,
        Constants.Step.VELOCITY_ITERATIONS,
        Constants.Step.POSITION_ITERATIONS);
    level.render();
  }

  @Override
  public void resize(final int width, final int height) {
    if (start) {
      level.resize(width, height);
    } else {
      splash.resize(width, height);
    }
    super.resize(width, height);
  }

  private void createSplash() {
    splash = new Splash(inputManager, spriteLoader);
    splash.create();
    resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
  }

  private void createLevel() {
    level = new FreePlay(spriteLoader, Level.createDefaultWorld(), inputManager);
    level.create();
    resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
  }
}
