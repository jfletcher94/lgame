package com.jfletcher.lobster.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jfletcher.lobster.audio.MusicManager;
import com.jfletcher.lobster.level.spawnstate.SpawnState;
import com.jfletcher.lobster.physical.InLevel;
import com.jfletcher.lobster.physical.contact.ContactParser;
import com.jfletcher.lobster.physical.contact.LContact;
import com.jfletcher.lobster.physical.environment.friendly.powerup.PowerUp;
import com.jfletcher.lobster.physical.nonplayer.hermitcrab.HermitCrab;
import com.jfletcher.lobster.physical.nonplayer.spider.Spider;
import com.jfletcher.lobster.physical.player.lobster.Lobster;
import com.jfletcher.lobster.physical.player.lobster.Score;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.InputManager;
import com.jfletcher.lobster.util.Logger;
import com.jfletcher.lobster.visual.particle.VisualElement;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;
import com.jfletcher.lobster.visual.text.InGameScoreDisplay;
import com.jfletcher.lobster.visual.text.TextElement;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FreePlay extends Level {

  private static final Logger logger = new Logger(FreePlay.class, Logger.Level.INFO);

  private static final String BACKGROUND = "textures/tank.png";
  private static final float BACKGROUND_SCALE = 5f * Constants.Scene.WORLD_TO_SCREEN;

  // Instance variables
  private Lobster lobster = null;
  private final MusicManager musicManager;
  private final List<Spider> spiders;
  private final List<HermitCrab> hermitCrabs;
  private final List<InLevel> staticObjects;
  private final List<InLevel> toDestroy;
  private final Set<Runnable> runAtEndOfStep;
  private final List<PowerUp> powerUps;
  private final List<VisualElement> visualElementsBack;
  private final List<VisualElement> visualElementsFront;
  private final List<TextElement> textElements;
  private final SpriteLoader spriteLoader;
  private final Sprite background;
  // TODO add uiSpriteBath to parent class
  private final Camera uiCamera;
  private final Viewport uiViewport;
  private final SpriteBatch uiSpriteBatch;
  private final Stage stage;
  private final InGameScoreDisplay inGameScoreDisplay;

  // State variables
  private long steps = 0L;
  private SpawnState spawnState;

  public FreePlay(
      final SpriteLoader spriteLoader, final World world, final InputManager inputManager) {
    super(world, inputManager);
    this.spriteLoader = spriteLoader;
    musicManager = MusicManager.getInstance();
    toDestroy = new ArrayList<>();
    runAtEndOfStep = new HashSet<>();
    spiders = new LinkedList<>();
    hermitCrabs = new LinkedList<>();
    staticObjects = new ArrayList<>();
    powerUps = new ArrayList<>();
    visualElementsBack = new ArrayList<>();
    visualElementsFront = new ArrayList<>();
    textElements = new ArrayList<>();
    spawnState = new SpawnState(this);
    uiCamera = new OrthographicCamera();
    uiViewport = new FitViewport(Constants.Scene.UI_SIZE.x, Constants.Scene.UI_SIZE.y, uiCamera);
    uiSpriteBatch = new SpriteBatch();
    uiSpriteBatch.setProjectionMatrix(uiCamera.combined);
    stage = new Stage(uiViewport, uiSpriteBatch);
    inGameScoreDisplay =
        new InGameScoreDisplay(this, this::getBestKillText, this::getScoreText, this::getKillsText);
    makeLevelEnclosed();
    world.setContactListener(this);
    background = createBackgroundVisuals();
  }

  @Override
  public void create() {
    setLobster(new Vector2(0f, -2.25f), new Vector2(0f, 14f));
  }

  @Override
  public void setLobster(final Vector2 position, final Vector2 velocity) {
    if (lobster != null) {
      return;
    }

    lobster = new Lobster();
    lobster.create(spriteLoader, this, position, velocity);
  }

  @Override
  public Vector2 getDefaultGravity() {
    return Vector2.Zero.cpy();
  }

  @Override
  public SpriteLoader getSpriteLoader() {
    return spriteLoader;
  }

  @Override
  public Stage getStage() {
    return stage;
  }

  @Override
  public Lobster getLobster() {
    return lobster;
  }

  @Override
  public List<Spider> getSpiders() {
    return spiders;
  }

  @Override
  public List<HermitCrab> getHermitCrabs() {
    return hermitCrabs;
  }

  @Override
  public List<InLevel> getStaticObjects() {
    return staticObjects;
  }

  @Override
  public List<VisualElement> getVisualElementsBack() {
    return visualElementsBack;
  }

  @Override
  public List<VisualElement> getVisualElementsFront() {
    return visualElementsFront;
  }

  @Override
  public List<TextElement> getTextElements() {
    return textElements;
  }

  @Override
  public List<InLevel> getDynamicObjects() {
    return Stream.concat(super.getDynamicObjects().stream(), powerUps.stream())
        .collect(Collectors.toList());
  }

  @Override
  public void destroy(TextElement textElement) {
    runAtEndOfStep.add(() -> getTextElements().remove(textElement));
    runAtEndOfStep.add(textElement.getActor()::remove);
  }

  @Override
  public void destroy(VisualElement visualElement) {
    runAtEndOfStep.add(() -> getVisualElementsFront().remove(visualElement));
    runAtEndOfStep.add(() -> getVisualElementsBack().remove(visualElement));
  }

  @Override
  public void destroy(final InLevel inLevel) {
    toDestroy.add(inLevel);
    if (inLevel instanceof Spider) {
      runAtEndOfStep.add(() -> spiders.remove(inLevel));
      return;
    }
    if (inLevel instanceof PowerUp) {
      runAtEndOfStep.add(() -> powerUps.remove(inLevel));
      return;
    }
    if (inLevel instanceof HermitCrab) {
      runAtEndOfStep.add(() -> hermitCrabs.remove(inLevel));
    }
  }

  @Override
  public Collection<InLevel> getToDestroys() {
    return toDestroy;
  }

  @Override
  public void addPowerUp(final PowerUp powerUp) {
    powerUps.add(powerUp);
  }

  @Override
  public void step(
      final float timeStep, final int velocityIterations, final int positionIterations) {
    if (getInputManager().consumeMute()) {
      musicManager.toggle();
    }
    if (getMenu() != null) {
      getMenu().step();
    }
    if (isHold()) {
      return;
    }


    super.step(timeStep, velocityIterations, positionIterations);
    steps++;
    if (!lobster.isDestroyed()) {
      spawnState.spawn(spriteLoader);
    }
    runAtEndOfStep.forEach(Runnable::run);
    runAtEndOfStep.clear();
  }

  @Override
  public void render() {
    Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    getSpriteBatch().setProjectionMatrix(getCamera().combined);
    getSpriteBatch().begin();
    background.draw(getSpriteBatch());
    getRenderables().forEach(renderable -> renderable.render(getSpriteBatch()));
    getSpriteBatch().end();
    getStage().draw();
    //    super.render();
  }

  @Override
  public void pause() {
    super.pause();
  }

  @Override
  public void resume() {
    super.resume();
  }

  @Override
  public void beginContact(final Contact contact) {
    final LContact lContact = ContactParser.parse(contact);
    if (lContact == null) {
      return;
    }

    lContact.apply();
  }

  @Override
  public void endContact(final Contact contact) {}

  @Override
  public void preSolve(final Contact contact, final Manifold oldManifold) {}

  @Override
  public void postSolve(final Contact contact, final ContactImpulse impulse) {}

  @Override
  public long getSteps() {
    return steps;
  }

  @Override
  public void dispose() {
    super.dispose();
    uiSpriteBatch.dispose();
    stage.dispose();
  }

  private Sprite createBackgroundVisuals() {
    final Sprite background = spriteLoader.createSprite(BACKGROUND);
    background.setOriginCenter();
    background.setOriginBasedPosition(0, 0);
    background.setScale(BACKGROUND_SCALE);
    return background;
  }

  public String getBestKillText() {
    return !lobster.isDestroyed() && lobster.getHighestScoreIncrement() == 0
        ? "Best Kill"
        : Score.decimalFormat.format(lobster.getHighestScoreIncrement());
  }

  public String getScoreText() {
    return !lobster.isDestroyed() && getLobster().getScore() == 0
        ? "Score"
        : Score.decimalFormat.format(getLobster().getScore());
  }

  public String getKillsText() {
    return !lobster.isDestroyed() && getLobster().getKills() == 0
        ? "Kills"
        : Score.decimalFormat.format(getLobster().getKills());
  }
}
