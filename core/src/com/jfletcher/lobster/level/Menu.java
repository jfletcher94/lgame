package com.jfletcher.lobster.level;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.jfletcher.lobster.audio.SoundEffectManager;
import com.jfletcher.lobster.util.InputManager;
import org.apache.commons.lang3.tuple.Pair;

public class Menu {

  public static final int PERIOD = 377; // whole number period: 120 * pi ~= 376.99
  public static final float BOUNCE_SPEED = 15f; // must be a factor of 120
  private final InputManager inputManager;
  private final SoundEffectManager soundEffectManager;
  private final Runnable exitRunnable;
  private final Table parent;
  private final Table menu;
  private final Label[] menuItems;

  private int selection;
  private int frames = 0;

  public Menu(
      final InputManager inputManager,
      final SoundEffectManager soundEffectManager,
      final Table parent,
      final Runnable exitRunnable,
      final Label.LabelStyle labelStyle,
      final float padding,
      final Pair<String, Runnable>... menuElements) {
    this.inputManager = inputManager;
    this.soundEffectManager = soundEffectManager;
    this.exitRunnable = exitRunnable;
    this.parent = parent;
    menu = new Table();
    menuItems = new Label[menuElements.length];
    for (int i = 0; i < menuElements.length; i++) {
      menuItems[i] = new Label(menuElements[i].getLeft(), labelStyle);
      menuItems[i].setUserObject(menuElements[i].getRight());
      menu.add(menuItems[i]);
      menu.padBottom(padding);
      menu.row();
    }
    parent.add(menu).colspan(parent.getColumns());
    selection = 0;
  }

  public Table getParent() {
    return parent;
  }

  public void step() {
    if (inputManager.consumeExit()) {
      exitRunnable.run();
      return;
    }

    if (inputManager.consumeUp()) {
      soundEffectManager.play(SoundEffectManager.SoundEffect.SELECT);
      getSelected().setFontScale(1);
      selection = Math.floorMod(selection - 1, menuItems.length);
    } else if (inputManager.consumeDown()) {
      soundEffectManager.play(SoundEffectManager.SoundEffect.SELECT);
      getSelected().setFontScale(1);
      selection = (selection + 1) % menuItems.length;
    }
    if (inputManager.consumeSelect()) {
      getSelectedRunnable().run();
    }
    if (++frames == PERIOD) {
      frames = 0;
    }
    getSelected().setFontScale(1f + 0.5f * (Math.abs((float) Math.sin((frames / BOUNCE_SPEED)))));
  }

  private Label getSelected() {
    return menuItems[selection];
  }

  private Runnable getSelectedRunnable() {
    return (Runnable) getSelected().getUserObject();
  }
}
