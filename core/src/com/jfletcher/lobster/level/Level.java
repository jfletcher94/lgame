package com.jfletcher.lobster.level;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.Renderable;
import com.jfletcher.lobster.audio.SoundEffectManager;
import com.jfletcher.lobster.physical.InLevel;
import com.jfletcher.lobster.physical.Steppable;
import com.jfletcher.lobster.physical.environment.friendly.powerup.PowerUp;
import com.jfletcher.lobster.physical.environment.neutral.Floor;
import com.jfletcher.lobster.physical.environment.neutral.Wall;
import com.jfletcher.lobster.physical.nonplayer.hermitcrab.HermitCrab;
import com.jfletcher.lobster.physical.nonplayer.spider.Spider;
import com.jfletcher.lobster.physical.player.lobster.Lobster;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.InputManager;
import com.jfletcher.lobster.visual.StyleManager;
import com.jfletcher.lobster.visual.particle.VisualElement;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;
import com.jfletcher.lobster.visual.text.TextElement;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class Level extends ApplicationAdapter implements Steppable, ContactListener {

  public interface Const {
    Vector2 STANDARD_GRAVITY = new Vector2(0.0f, -16.0f);
  }

  public static World createDefaultWorld() {
    return new World(Vector2.Zero, true);
  }

  private final World world;
  private final InputManager inputManager;
  private final OrthographicCamera camera;
  private final Viewport viewport;
  private final SpriteBatch spriteBatch;
  private final Box2DDebugRenderer debugRenderer;
  private final SoundEffectManager soundEffectManager;

  private Menu menu = null;
  private boolean restart = false;
  private boolean hold = false;

  public Level(final World world, final InputManager inputManager) {
    this.world = world;
    this.inputManager = inputManager;
    camera = new OrthographicCamera();
    viewport = new FitViewport(Constants.Scene.SIZE.x, Constants.Scene.SIZE.y, camera);
    spriteBatch = new SpriteBatch();
    debugRenderer = new Box2DDebugRenderer();
    world.setGravity(getDefaultGravity());
    soundEffectManager = new SoundEffectManager();
  }

  public Menu getMenu() {
    return menu;
  }

  public void setMenu(final Menu menu) {
    inputManager.clear();
    this.menu = menu;
  }

  public InputManager getInputManager() {
    return inputManager;
  }

  public SoundEffectManager getSoundEffectManager() {
    return soundEffectManager;
  }

  public abstract void setLobster(final Vector2 position, final Vector2 velocity);

  public abstract Vector2 getDefaultGravity();

  public abstract SpriteLoader getSpriteLoader();

  public abstract Stage getStage();

  public abstract Lobster getLobster();

  public abstract List<Spider> getSpiders();

  public abstract List<HermitCrab> getHermitCrabs();

  public abstract List<InLevel> getStaticObjects();

  public abstract List<VisualElement> getVisualElementsFront();

  public abstract List<VisualElement> getVisualElementsBack();

  public abstract List<TextElement> getTextElements();

  public abstract void destroy(TextElement textElement);

  public abstract void destroy(VisualElement visualElement);

  public abstract void destroy(final InLevel inLevel);

  public abstract Collection<InLevel> getToDestroys();

  public abstract long getSteps();

  public boolean checkStep(final int step) {
    return step < 0 || getSteps() % step == 0;
  }

  public void addSpider(final Spider spider) {
    getSpiders().add(spider);
  }

  public void addHermitCrab(final HermitCrab hermitCrab) {
    getHermitCrabs().add(hermitCrab);
  }

  public abstract void addPowerUp(final PowerUp powerUp);

  public void addStaticObject(final InLevel staticObject) {
    getStaticObjects().add(staticObject);
  }

  public void addVisualElementFront(final VisualElement visualElement) {
    getVisualElementsFront().add(visualElement);
  }

  public void addVisualElementBack(final VisualElement visualElement) {
    getVisualElementsBack().add(visualElement);
  }

  public void addTextElement(final TextElement textElement) {
    getTextElements().add(textElement);
    getStage().addActor(textElement.getActor());
  }

  public World getWorld() {
    return world;
  }

  public Camera getCamera() {
    return camera;
  }

  public Viewport getViewport() {
    return viewport;
  }

  public SpriteBatch getSpriteBatch() {
    return spriteBatch;
  }

  public List<InLevel> getDynamicObjects() {
    return Stream.of(getSpiders(), List.of(getLobster()), getHermitCrabs())
        .flatMap(Collection::stream)
        .collect(Collectors.toList());
  }

  public List<Renderable> getRenderables() {
    return Stream.of(
            getVisualElementsBack(),
            getDynamicObjects(),
            getStaticObjects(),
            getVisualElementsFront())
        .flatMap(Collection::stream)
        .collect(Collectors.toList());
  }

  public Collection<Disposable> getDisposables() {
    // TODO add visualElements
    return Stream.concat(getDynamicObjects().stream(), getStaticObjects().stream())
        .collect(Collectors.toList());
  }

  public void restart() {
    restart = true;
  }

  public boolean toRestart() {
    return restart;
  }

  @Override
  public void render() {
    debugRenderer.render(world, camera.combined);
  }

  @Override
  public void step(
      final float timeStep, final int velocityIterations, final int positionIterations) {
//    scroll();
    getTextElements()
        .forEach(textElement -> textElement.step(timeStep, velocityIterations, positionIterations));
    getVisualElementsBack()
        .forEach(
            visualElement -> visualElement.step(timeStep, velocityIterations, positionIterations));
    getVisualElementsFront()
        .forEach(
            visualElement -> visualElement.step(timeStep, velocityIterations, positionIterations));
    getDynamicObjects()
        .forEach(inWorld -> inWorld.step(timeStep, velocityIterations, positionIterations));
    getToDestroys().stream()
        .map(InLevel::getBody)
        .peek(body -> body.setUserData(null))
        .forEach(getWorld()::destroyBody);
    getToDestroys().clear();
    getWorld().step(timeStep, velocityIterations, positionIterations);

    if (!getLobster().isDead() && inputManager.consumeExit()) {
      hold();
    }
  }

  protected void hold() {
    soundEffectManager.play(SoundEffectManager.SoundEffect.PAUSE);
    hold = true;
    setMenu(
        new Menu(
            inputManager,
            soundEffectManager,
            new Table(),
            this::unHold,
            StyleManager.createLabelStyle(24, Color.BLACK, 2, Color.WHITE),
            0f,
            Pair.of("Resume", this::unHold),
            Pair.of("Exit", Gdx.app::exit)));
    menu.getParent().setPosition(Constants.Scene.UI_HALF_SIZE.x, Constants.Scene.UI_HALF_SIZE.y);
    getStage().addActor(menu.getParent());
  }

  protected void unHold() {
    soundEffectManager.play(SoundEffectManager.SoundEffect.PAUSE);
    hold = false;
    menu.getParent().remove();
    setMenu(null);
  }

  protected boolean isHold() {
    return hold;
  }

  @Override
  public void resize(final int width, final int height) {
    getViewport().update(width, height, false);
  }

  @Override
  public void dispose() {
    getDisposables().forEach(Disposable::dispose);
    getSpiders().clear();
    getStaticObjects().clear();
    getDynamicObjects().clear();
    getToDestroys().clear();
    getSpriteBatch().dispose();
  }

  protected Vector2 getLobsterCameraDelta() {
    return new Vector2(
        getLobster().getBody().getPosition().x - getCamera().position.x,
        getLobster().getBody().getPosition().y - getCamera().position.y);
  }

  protected void moveViewBy(final Vector2 delta) {}

  protected void scroll() {
    final Vector2 delta = getLobsterCameraDelta();
    if (canScrollLeft() && delta.x < getLeftScrollThreshold()) {
      moveViewBy(new Vector2(delta.x - getLeftScrollThreshold(), 0.0f));
    } else if (canScrollRight() && delta.x > getRightScrollThreshold()) {
      moveViewBy(new Vector2(delta.x - getRightScrollThreshold(), 0.0f));
    }

    if (canScrollUp() && delta.y > getUpScrollThreshold()) {
      moveViewBy(new Vector2(0.0f, delta.y - getUpScrollThreshold()));
    } else if (canScrollDown() && delta.y < getDownScrollThreshold()) {
      moveViewBy(new Vector2(0.0f, delta.y - getDownScrollThreshold()));
    }
  }

  protected boolean canScrollLeft() {
    return false;
  }

  protected float getLeftScrollThreshold() {
    throw new UnsupportedOperationException();
  }

  public void setLeftScrollThreshold(final float rightScrollThreshold) {
    throw new UnsupportedOperationException();
  }

  protected boolean canScrollRight() {
    return false;
  }

  protected float getRightScrollThreshold() {
    throw new UnsupportedOperationException();
  }

  public void setRightScrollThreshold(final float rightScrollThreshold) {
    throw new UnsupportedOperationException();
  }

  protected boolean canScrollUp() {
    return false;
  }

  protected float getUpScrollThreshold() {
    throw new UnsupportedOperationException();
  }

  public void setUpScrollThreshold(final float rightScrollThreshold) {
    throw new UnsupportedOperationException();
  }

  protected boolean canScrollDown() {
    return false;
  }

  protected float getDownScrollThreshold() {
    throw new UnsupportedOperationException();
  }

  public void setDownScrollThreshold(final float rightScrollThreshold) {
    throw new UnsupportedOperationException();
  }

  @VisibleForTesting
  protected void makeLevelEnclosed() {
    createGround(BodyDef.BodyType.StaticBody);
    createCeiling(BodyDef.BodyType.StaticBody);
    createLeftWall(BodyDef.BodyType.StaticBody);
    createRightWall(BodyDef.BodyType.StaticBody);
  }

  @VisibleForTesting
  Body createGround(final BodyDef.BodyType bodyType) {
    final Body body = createHorizontalWall(-Constants.Scene.HALF_SIZE.y, bodyType);
    final Floor floor = new Floor();
    floor.create(body);
    return body;
  }

  @VisibleForTesting
  Body createCeiling(final BodyDef.BodyType bodyType) {
    final Body body = createHorizontalWall(Constants.Scene.HALF_SIZE.y, bodyType);
    final Wall wall = new Wall();
    wall.create(body);
    return body;
  }

  @VisibleForTesting
  Body createLeftWall(final BodyDef.BodyType bodyType) {
    final Body body = createVerticalWall(-Constants.Scene.HALF_SIZE.x, bodyType);
    final Wall wall = new Wall();
    wall.create(body);
    return body;
  }

  @VisibleForTesting
  Body createRightWall(final BodyDef.BodyType bodyType) {
    final Body body = createVerticalWall(Constants.Scene.HALF_SIZE.x, bodyType);
    final Wall wall = new Wall();
    wall.create(body);
    return body;
  }

  @VisibleForTesting
  Body createHorizontalWall(final float y, final BodyDef.BodyType bodyType) {
    return createWall(-Constants.Scene.HALF_SIZE.x, y, Constants.Scene.SIZE.x, 0.0f, bodyType);
  }

  @VisibleForTesting
  Body createVerticalWall(final float x, final BodyDef.BodyType bodyType) {
    return createWall(x, -Constants.Scene.HALF_SIZE.y, 0.0f, Constants.Scene.SIZE.y, bodyType);
  }

  @VisibleForTesting
  Body createWall(
      final float x1,
      final float y1,
      final float x2,
      final float y2,
      final BodyDef.BodyType bodyType) {
    final BodyDef groundBodyDef = new BodyDef();
    groundBodyDef.type = bodyType;
    groundBodyDef.position.set(x1, y1);
    final Body body = getWorld().createBody(groundBodyDef);
    PolygonShape groundBox = new PolygonShape();
    groundBox.setAsBox(x2, y2);
    body.createFixture(groundBox, 0.0f);
    groundBox.dispose();
    body.setUserData(this);
    return body;
  }
}
