package com.jfletcher.lobster.level;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jfletcher.lobster.audio.MusicManager;
import com.jfletcher.lobster.audio.SoundEffectManager;
import com.jfletcher.lobster.hud.ScoreMenu;
import com.jfletcher.lobster.physical.Steppable;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.InputManager;
import com.jfletcher.lobster.visual.StyleManager;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;
import org.apache.commons.lang3.tuple.Pair;

public class Splash extends ApplicationAdapter implements Steppable {

  private static final String BACKGROUND = "textures/tank.png";
  private static final float BACKGROUND_SCALE = 5f;

  private final MusicManager musicManager;
  private final SpriteLoader spriteLoader;
  private final InputManager inputManager;
  private final SoundEffectManager soundEffectManager;
  private final Sprite background;
  private OrthographicCamera camera;
  private Viewport viewport;
  private SpriteBatch spriteBatch;
  private Stage stage;
  private Menu mainMenu;
  private Menu controlsMenu;
  private Menu highScoreMenu;

  private Menu menu;
  private boolean start = false;

  public Splash(final InputManager inputManager, final SpriteLoader spriteLoader) {
    this.musicManager = MusicManager.getInstance();
    this.spriteLoader = spriteLoader;
    this.inputManager = inputManager;
    this.background = createBackgroundVisuals();
    this.soundEffectManager = new SoundEffectManager();
  }

  @Override
  public void create() {
    camera = new OrthographicCamera();
    viewport = new FitViewport(Constants.Scene.UI_SIZE.x, Constants.Scene.UI_SIZE.y, camera);
    spriteBatch = new SpriteBatch();
    stage = new Stage(viewport, spriteBatch);

    final Label.LabelStyle labelStyle =
        StyleManager.createLabelStyle(24, Color.BLACK, 2, Color.WHITE);
    final Table controlsTable = new Table();
    controlsTable.setPosition(
        Constants.Scene.UI_HALF_SIZE.x, Constants.Scene.UI_SIZE.y * 0.4f, Align.center);
    controlsTable.add(new Label("Start/Restart: ", labelStyle)).left();
    controlsTable.add(new Label("Space", labelStyle)).right();
    controlsTable.row().padBottom(8f);
    controlsTable.add(new Label("Move: ", labelStyle)).left();
    controlsTable.add(new Label("WASD or Arrow Keys", labelStyle)).right();
    controlsTable.row().padBottom(8f);
    controlsTable.add(new Label("Pause/Back: ", labelStyle)).left();
    controlsTable.add(new Label("Esc", labelStyle)).right();
    controlsTable.row().padBottom(8f);
    controlsTable.add(new Label("Toggle Music: ", labelStyle)).left();
    controlsTable.add(new Label("M", labelStyle)).right();
    controlsTable.row().padBottom(12f);
    controlsMenu =
        new Menu(
            inputManager,
            soundEffectManager,
            controlsTable,
            this::setMainMenu,
            StyleManager.createLabelStyle(18, Color.BLACK, 2, Color.WHITE),
            0f,
            Pair.of("Back", this::setMainMenu));

    final ScoreMenu scoreMenu = new ScoreMenu();
    scoreMenu
        .getTable()
        .setPosition(
            Constants.Scene.UI_HALF_SIZE.x, Constants.Scene.UI_SIZE.y * 0.4f, Align.center);
    highScoreMenu =
        new Menu(
            inputManager,
            soundEffectManager,
            scoreMenu.getTable(),
            this::setMainMenu,
            StyleManager.createLabelStyle(18, Color.BLACK, 2, Color.WHITE),
            0f,
            Pair.of("Back", this::setMainMenu));
    mainMenu =
        new Menu(
            inputManager,
            soundEffectManager,
            new Table(),
            Gdx.app::exit,
            StyleManager.createLabelStyle(32, Color.BLACK, 3, Color.WHITE),
            12f,
            Pair.of("Play", this::start),
            Pair.of("Controls", this::setControlsMenu),
            Pair.of("High Scores", this::setHighScoreMenu),
            Pair.of("Quit", Gdx.app::exit));
    mainMenu
        .getParent()
        .setPosition(
            Constants.Scene.UI_HALF_SIZE.x, Constants.Scene.UI_SIZE.y * 0.4f, Align.center);
    menu = mainMenu;
    stage.addActor(menu.getParent());

    final Label title =
        new Label(
            "Lobster Roll",
            StyleManager.createLabelStyle(64, new Color(0.8f, 0f, 0f, 1f), 4, Color.WHITE));
    title.setPosition(
        Constants.Scene.UI_HALF_SIZE.x, Constants.Scene.UI_SIZE.y * 0.8f, Align.center);
    final Label attribution =
        new Label(
            "Surf Shimmy by Kevin MacLeod\n"
                + "Link: https://incompetech.filmmusic.io/song/4448-surf-shimmy\n"
                + "License: https://creativecommons.org/licenses/by/4.0/",
            StyleManager.createLabelStyle(12, Color.BLACK, 1, Color.WHITE));
    attribution.setPosition(16f, 16f, Align.bottomLeft);
    final Label info =
        new Label(
            "A game by Jonathan Fletcher\n" + "https://jfletcher94.itch.io/lobster-roll",
            StyleManager.createLabelStyle(12, Color.BLACK, 1, Color.WHITE));
    info.setPosition(Constants.Scene.UI_SIZE.x - 16f, 16f, Align.bottomRight);
    stage.addActor(title);
    stage.addActor(attribution);
    stage.addActor(info);
  }

  @Override
  public void resize(final int width, final int height) {
    viewport.update(width, height, false);
  }

  @Override
  public void render() {
    Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    spriteBatch.setProjectionMatrix(camera.combined);
    spriteBatch.begin();
    background.draw(spriteBatch);
    spriteBatch.end();
    stage.draw();
  }

  @Override
  public void pause() {}

  @Override
  public void resume() {}

  @Override
  public void dispose() {
    spriteBatch.dispose();
    stage.dispose();
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    menu.step();
    if (inputManager.consumeMute()) {
      musicManager.toggle();
    }
  }

  public boolean isStart() {
    return start;
  }

  private Sprite createBackgroundVisuals() {
    final Sprite background = spriteLoader.createSprite(BACKGROUND);
    background.setOriginCenter();
    background.setOriginBasedPosition(
        Constants.Scene.UI_HALF_SIZE.x, Constants.Scene.UI_HALF_SIZE.y);
    background.setScale(BACKGROUND_SCALE);
    return background;
  }

  private void start() {
    inputManager.clear();
    soundEffectManager.play(SoundEffectManager.SoundEffect.START);
    start = true;
  }

  private void setMainMenu() {
    setMenu(mainMenu);
  }

  private void setControlsMenu() {
    setMenu(controlsMenu);
  }

  private void setHighScoreMenu() {
    setMenu(highScoreMenu);
  }

  private void setMenu(final Menu menu) {
    this.menu.getParent().remove();
    this.menu = menu;
    stage.addActor(menu.getParent());
  }
}
