package com.jfletcher.lobster.level.spawnstate;

import com.badlogic.gdx.math.Vector2;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.environment.friendly.powerup.PowerUp;
import com.jfletcher.lobster.physical.nonplayer.hermitcrab.HermitCrab;
import com.jfletcher.lobster.physical.nonplayer.spider.Spider;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.Logger;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

public class SpawnState {

  private static final Logger logger = new Logger(SpawnState.class, Logger.Level.WARN);

  private interface Const {

    interface HermitCrabs {
      int PROBABILITY = 1500;
      int MAX_COUNT = 12;
      int INITIAL_CAP = 0;
      int KILLS_PER_CAP = 25;
    }

    interface PowerUps {
      int BASE_PROBABILITY = 500;
      int MAX_PROBABILITY = 200;
      int LOBSTER_KILL_SCALAR = 4;
      float SPAWN_X_MIN = -Constants.Scene.HALF_SIZE.x + 1.0f;
      float SPAWN_X_MAX = Constants.Scene.HALF_SIZE.x - 1.0f;
      float SPAWN_Y_MIN = -Constants.Scene.HALF_SIZE.y + 1.0f;
      float SPAWN_Y_MAX = Constants.Scene.HALF_SIZE.y - 1.0f;
    }

    interface Spiders {
      int MAX_COUNT = 12;
      int MAX_SPAWN_RETRIES = 24;
      float SPAWN_POSITION_X_MIN = Constants.Scene.HALF_SIZE.x * -0.9f;
      float SPAWN_POSITION_X_MAX = Constants.Scene.HALF_SIZE.x * 0.9f;
      float SPAWN_POSITION_Y = Constants.Scene.HALF_SIZE.y * 0.98f;
      float SPAWN_SPEED_MIN = -4.0f;
      float SPAWN_SPEED_MAX = -3.0f;
      float MIN_SPAWN_SEPARATION = 2.0f * Spider.Const.Body.RADIUS;
      float LOBSTER_KILL_SCALAR = -0.01f;
      float SPIDER_SPEED_MAX = SPAWN_SPEED_MIN * 4.0f;
    }

    interface Step {
      // Number of seconds * SCALE
      int SPIDER_SPAWN = (int) (1.33f * Constants.Step.STEPS_PER_SECOND);
      int KILL_COUNT_SCALAR = 6;
      int SPIDER_SPAWN_MIN = SPIDER_SPAWN / 6;
    }
  }

  private final Level level;

  public SpawnState(final Level level) {
    this.level = level;
  }

  public void spawn(final SpriteLoader spriteLoader) {
    spawnSpider(spriteLoader);
    spawnHermitCrab(spriteLoader);
    spawnPowerUp(spriteLoader);
  }

  @VisibleForTesting
  void spawnSpider(final SpriteLoader spriteLoader) {
    if (level.getSpiders().size() < Const.Spiders.MAX_COUNT
        && level.checkStep(
            Math.max(
                Const.Step.SPIDER_SPAWN_MIN,
                Const.Step.SPIDER_SPAWN
                    - (level.getLobster().getKills() / Const.Step.KILL_COUNT_SCALAR)))) {
      final Spider spider = new Spider();
      spider.create(spriteLoader, level, findSpiderSpawnLocation(), calculateSpiderSpawnSpeed());
      level.getSpiders().add(spider);
    }
  }

  @VisibleForTesting
  void spawnHermitCrab(final SpriteLoader spriteLoader) {
    if (level.getHermitCrabs().size() < Const.HermitCrabs.MAX_COUNT
        && level.getHermitCrabs().size()
            < Const.HermitCrabs.INITIAL_CAP
                + level.getLobster().getKills() / Const.HermitCrabs.KILLS_PER_CAP
        && RandomUtil.oneIn(Const.HermitCrabs.PROBABILITY)) {
      final HermitCrab hermitCrab = new HermitCrab();
      hermitCrab.create(spriteLoader, level, RandomUtil.oneIn(2));
      level.addHermitCrab(hermitCrab);
    }
  }

  @VisibleForTesting
  void spawnPowerUp(final SpriteLoader spriteLoader) {
    if (RandomUtil.oneIn(
        Math.max(
            Const.PowerUps.MAX_PROBABILITY,
            Const.PowerUps.BASE_PROBABILITY
                - level.getLobster().getKills() / Const.PowerUps.LOBSTER_KILL_SCALAR))) {
      PowerUp powerUp = null;
      int i = 0;
      final int result = RandomUtil.intRange(0, PowerUp.Type.TOTAL_PROBABILITY);
      for (final PowerUp.Type type : PowerUp.Type.values()) {
        i += type.getProbability();
        if (i > result) {
          powerUp = type.getSupplier().get();
          break;
        }
      }
      if (powerUp == null) {
        logger.error("PowerUp not initialized!");
        assert false;
      }

      powerUp.create(
          spriteLoader,
          level,
          new Vector2(
              RandomUtil.floatRange(Const.PowerUps.SPAWN_X_MIN, Const.PowerUps.SPAWN_X_MAX),
              RandomUtil.floatRange(Const.PowerUps.SPAWN_Y_MIN, Const.PowerUps.SPAWN_Y_MAX)));
      level.addPowerUp(powerUp);
    }
  }

  @VisibleForTesting
  Vector2 findSpiderSpawnLocation() {
    int tries = 0;
    final Vector2 vector2 = new Vector2();
    do {
      vector2.set(
          RandomUtil.floatRange(
              Const.Spiders.SPAWN_POSITION_X_MIN, Const.Spiders.SPAWN_POSITION_X_MAX),
          Const.Spiders.SPAWN_POSITION_Y);
    } while (tries++ < Const.Spiders.MAX_SPAWN_RETRIES
        && (checkLobster(vector2) || checkExistingSpiders(vector2)));
    if (tries > Const.Spiders.MAX_SPAWN_RETRIES) {
      logger.warn(
          String.format(
              "Tried and failed to add spider %d times!", Const.Spiders.MAX_SPAWN_RETRIES));
    }
    return vector2;
  }

  @VisibleForTesting
  Vector2 calculateSpiderSpawnSpeed() {
    return new Vector2(
        0.0f,
        Math.max(
            Const.Spiders.SPIDER_SPEED_MAX,
            RandomUtil.floatRange(Const.Spiders.SPAWN_SPEED_MIN, Const.Spiders.SPAWN_SPEED_MAX)
                + level.getLobster().getKills() * Const.Spiders.LOBSTER_KILL_SCALAR));
  }

  @VisibleForTesting
  boolean checkLobster(final Vector2 vector2) {
    return Math.abs(level.getLobster().getBody().getPosition().x - vector2.x)
        < Const.Spiders.MIN_SPAWN_SEPARATION;
  }

  @VisibleForTesting
  boolean checkExistingSpiders(final Vector2 vector2) {
    return level.getSpiders().stream()
        .map(Spider::getPosition)
        .anyMatch(
            position -> Math.abs(position.x - vector2.x) < Const.Spiders.MIN_SPAWN_SEPARATION);
  }
}
