package com.jfletcher.lobster.util;

public class MathUtil {

  public static int compare(final float f1, final float f2) {
    return compare(f1, f2, Float.MIN_NORMAL);
  }

  public static int compare(final float f1, final float f2, final float epsilon) {
    return Math.abs(f1 - f2) < epsilon ? 0 : Float.compare(f1, f2);
  }
}
