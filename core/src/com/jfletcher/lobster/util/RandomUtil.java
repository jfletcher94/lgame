package com.jfletcher.lobster.util;

import com.google.common.base.Suppliers;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class RandomUtil {

  private static final Supplier<Random> random = Suppliers.memoize(Random::new);

  public static float floatRange(final float min, final float max) {
    return min + random.get().nextFloat() * (max - min);
  }

  public static int intRange(final int min, final int max) {
    return min + random.get().nextInt(max - min);
  }

  public static boolean oneIn(final int n) {
    return random.get().nextInt(n) == 0;
  }

  public static <T> T randomElement(final List<T> list) {
    return list.get(random.get().nextInt(list.size()));
  }
}
