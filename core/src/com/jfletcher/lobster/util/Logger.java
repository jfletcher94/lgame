package com.jfletcher.lobster.util;

import java.util.function.Supplier;

public class Logger {

  private static final String LOG_FORMAT = "%s [%s] %s: %s";

  public enum Level {
    FATAL(0),
    ERROR(1),
    WARN(2),
    INFO(3),
    DEBUG(4);

    private final int level;

    Level(final int level) {
      this.level = level;
    }
  }

  private final String tag;
  private Level level;

  public Logger(final Class<?> clazz) {
    this(clazz.getName());
  }

  public Logger(final Class<?> clazz, final Level level) {
    this(clazz.getName(), level);
  }

  public Logger(final String tag) {
    this(tag, Level.ERROR);
  }

  public Logger(final String tag, final Level level) {
    this.tag = tag;
    this.level = level;
  }

  public void debug(final String message) {
    if (level.level >= Level.DEBUG.level) {
      System.out.printf((LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.DEBUG, tag, message);
    }
  }

  public void debug(final String message, final Exception exception) {
    if (level.level >= Level.DEBUG.level) {
      System.out.printf((LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.DEBUG, tag, message);
      exception.printStackTrace();
    }
  }

  public void debug(final Supplier<String> message) {
    if (level.level >= Level.DEBUG.level) {
      System.out.printf(
          (LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.DEBUG, tag, message.get());
    }
  }

  public void debug(final Supplier<String> message, final Exception exception) {
    if (level.level >= Level.DEBUG.level) {
      System.out.printf(
          (LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.DEBUG, tag, message.get());
      exception.printStackTrace();
    }
  }

  public void info(final String message) {
    if (level.level >= Level.INFO.level) {
      System.out.printf((LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.INFO, tag, message);
    }
  }

  public void info(final String message, final Exception exception) {
    if (level.level >= Level.INFO.level) {
      System.out.printf((LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.INFO, tag, message);
      exception.printStackTrace();
    }
  }

  public void info(final Supplier<String> message) {
    if (level.level >= Level.INFO.level) {
      System.out.printf(
          (LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.INFO, tag, message.get());
    }
  }

  public void info(final Supplier<String> message, final Exception exception) {
    if (level.level >= Level.INFO.level) {
      System.out.printf(
          (LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.INFO, tag, message.get());
      exception.printStackTrace();
    }
  }

  public void warn(final String message) {
    if (level.level >= Level.WARN.level) {
      System.out.printf((LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.WARN, tag, message);
    }
  }

  public void warn(final String message, final Exception exception) {
    if (level.level >= Level.WARN.level) {
      System.out.printf((LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.WARN, tag, message);
      exception.printStackTrace();
    }
  }

  public void warn(final Supplier<String> message) {
    if (level.level >= Level.WARN.level) {
      System.out.printf(
          (LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.WARN, tag, message.get());
    }
  }

  public void warn(final Supplier<String> message, final Exception exception) {
    if (level.level >= Level.WARN.level) {
      System.out.printf(
          (LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.WARN, tag, message.get());
      exception.printStackTrace();
    }
  }

  public void error(final String message) {
    if (level.level >= Level.ERROR.level) {
      System.out.printf((LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.ERROR, tag, message);
    }
  }

  public void error(final String message, Throwable exception) {
    if (level.level >= Level.ERROR.level) {
      System.out.printf((LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.ERROR, tag, message);
      exception.printStackTrace();
    }
  }

  public void error(final Supplier<String> message) {
    if (level.level >= Level.ERROR.level) {
      System.out.printf(
          (LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.ERROR, tag, message.get());
    }
  }

  public void error(final Supplier<String> message, final Exception exception) {
    if (level.level >= Level.ERROR.level) {
      System.out.printf(
          (LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.ERROR, tag, message.get());
      exception.printStackTrace();
    }
  }

  public void fatal(final String message) {
    if (level.level >= Level.FATAL.level) {
      System.out.printf((LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.FATAL, tag, message);
    }
  }

  public void fatal(final String message, Throwable exception) {
    if (level.level >= Level.FATAL.level) {
      System.out.printf((LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.FATAL, tag, message);
      exception.printStackTrace();
    }
  }

  public void fatal(final Supplier<String> message) {
    if (level.level >= Level.FATAL.level) {
      System.out.printf(
          (LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.FATAL, tag, message.get());
    }
  }

  public void fatal(final Supplier<String> message, final Exception exception) {
    if (level.level >= Level.FATAL.level) {
      System.out.printf(
          (LOG_FORMAT) + "%n", System.currentTimeMillis(), Level.FATAL, tag, message.get());
      exception.printStackTrace();
    }
  }

  public void setLevel(final Level level) {
    this.level = level;
  }

  public Level getLevel() {
    return level;
  }
}
