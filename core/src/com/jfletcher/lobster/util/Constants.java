package com.jfletcher.lobster.util;

import com.badlogic.gdx.math.Vector2;

public interface Constants {

  interface Scene {
    Vector2 SIZE = new Vector2(16.00f, 9.00f);
    Vector2 HALF_SIZE = SIZE.cpy().scl(0.5f);
    Vector2 UI_SIZE = new Vector2(1600f, 900f);
    Vector2 UI_HALF_SIZE = UI_SIZE.cpy().scl(0.5f);
    float WORLD_TO_SCREEN = 1.0f / 100.0f;
  }

  interface Step {
    int STEPS_PER_SECOND = 60;
    float TIME_STEP = 1.0f / STEPS_PER_SECOND;
    int VELOCITY_ITERATIONS = 6;
    int POSITION_ITERATIONS = 2;
  }
}
