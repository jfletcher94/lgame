package com.jfletcher.lobster.util;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jfletcher.lobster.physical.Steppable;

public class InputManager extends InputAdapter implements Steppable {

  public enum Axis {
    HORIZONTAL,
    VERTICAL
  }

  private static final float AXIS_MAX = 1f;
  private static final float AXIS_MIN = -AXIS_MAX;
  private static final float AXIS_INCREMENT = 8f * Constants.Step.TIME_STEP;

  private Stage stage = null;

  // State variables
  private boolean isLeftPressed = false,
      isRightPressed = false,
      isUpPressed = false,
      isDownPressed = false;
  private float horizontalAxis = 0f, verticalAxis = 0f;
  private boolean select = false, selectDown = false, exit = false, exitDown = false;
  private boolean up = false, down = false;
  private boolean mute = false, muteDown = false;

  public void setStage(final Stage stage) {
    this.stage = stage;
  }

  public void clear() {
    select = false;
    exit = false;
    up = false;
    down = false;
    mute = false;
  }

  public float getAxis(final Axis axis) {
    switch (axis) {
      case HORIZONTAL:
        return horizontalAxis;
      case VERTICAL:
        return verticalAxis;
      default:
        assert false;
        return 0f;
    }
  }

  public boolean consumeSelect() {
    if (select) {
      select = false;
      return true;
    }
    return false;
  }

  public boolean consumeExit() {
    if (exit) {
      exit = false;
      return true;
    }
    return false;
  }

  public boolean consumeUp() {
    if (up) {
      up = false;
      return true;
    }
    return false;
  }

  public boolean consumeDown() {
    if (down) {
      down = false;
      return true;
    }
    return false;
  }

  public boolean consumeMute() {
    if (mute) {
      mute = false;
      return true;
    }
    return false;
  }

  @Override
  public boolean keyTyped(char character) {
    return stage == null || stage.keyTyped(character);
  }

  @Override
  public boolean keyDown(final int key) {
    switch (key) {
      case Input.Keys.SPACE:
        if (!selectDown) {
          select = true;
          selectDown = true;
        }
        return true;
      case Input.Keys.ESCAPE:
        if (!exitDown) {
          exit = true;
          exitDown = true;
        }
        return true;
      case Input.Keys.M:
        if (stage != null) {
          return false;
        }
        if (!muteDown) {
          mute = true;
          muteDown = true;
        }
        return true;
      case Input.Keys.A:
        if (stage != null) {
          return false;
        }
      case Input.Keys.LEFT:
        isLeftPressed = true;
        return true;
      case Input.Keys.D:
        if (stage != null) {
          return false;
        }
      case Input.Keys.RIGHT:
        isRightPressed = true;
        return true;
      case Input.Keys.W:
        if (stage != null) {
          return false;
        }
      case Input.Keys.UP:
        up = true;
        isUpPressed = true;
        return true;
      case Input.Keys.S:
        if (stage != null) {
          return false;
        }
      case Input.Keys.DOWN:
        down = true;
        isDownPressed = true;
        return true;
      default:
        return false;
    }
  }

  @Override
  public boolean keyUp(final int key) {
    switch (key) {
      case Input.Keys.SPACE:
        select = false;
        selectDown = false;
        return true;
      case Input.Keys.ESCAPE:
        exit = false;
        exitDown = false;
        return true;
      case Input.Keys.M:
        mute = false;
        muteDown = false;
        return true;
      case Input.Keys.LEFT:
      case Input.Keys.A:
        isLeftPressed = false;
        return true;
      case Input.Keys.RIGHT:
      case Input.Keys.D:
        isRightPressed = false;
        return true;
      case Input.Keys.UP:
      case Input.Keys.W:
        isUpPressed = false;
        return true;
      case Input.Keys.DOWN:
      case Input.Keys.S:
        isDownPressed = false;
        return true;
      default:
        return false;
    }
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    // horizontal
    // apply user input
    if (isRightPressed && !isLeftPressed) {
      if (horizontalAxis < 0f) {
        horizontalAxis = 0f;
      }
      horizontalAxis += AXIS_INCREMENT;
    } else if (isLeftPressed) {
      if (horizontalAxis > 0f) {
        horizontalAxis = 0f;
      }
      horizontalAxis -= AXIS_INCREMENT;
    }
    // apply release of user input
    else {
      if (horizontalAxis < -AXIS_INCREMENT) {
        horizontalAxis += AXIS_INCREMENT;
      } else if (horizontalAxis > AXIS_INCREMENT) {
        horizontalAxis -= AXIS_INCREMENT;
      } else {
        horizontalAxis = 0f;
      }
    }
    // cap axis value
    if (horizontalAxis > AXIS_MAX - Float.MIN_NORMAL) {
      horizontalAxis = AXIS_MAX;
    } else if (horizontalAxis < AXIS_MIN + Float.MIN_NORMAL) {
      horizontalAxis = AXIS_MIN;
    }

    // vertical
    if (isUpPressed && !isDownPressed) {
      if (verticalAxis < 0f) {
        verticalAxis = 0f;
      }
      verticalAxis += AXIS_INCREMENT;
    } else if (isDownPressed) {
      if (verticalAxis > 0f) {
        verticalAxis = 0f;
      }
      verticalAxis -= AXIS_INCREMENT;
    } else {
      if (verticalAxis < -AXIS_INCREMENT - Float.MIN_NORMAL) {
        verticalAxis += AXIS_INCREMENT;
      } else if (verticalAxis > AXIS_INCREMENT + Float.MIN_NORMAL) {
        verticalAxis -= AXIS_INCREMENT;
      } else {
        verticalAxis = 0f;
      }
    }
    if (verticalAxis > AXIS_MAX) {
      verticalAxis = AXIS_MAX;
    } else if (verticalAxis < AXIS_MIN) {
      verticalAxis = AXIS_MIN;
    }
  }
}
