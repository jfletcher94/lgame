package com.jfletcher.lobster.visual.sprite;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.google.common.annotations.VisibleForTesting;

@Singleton
public class SpriteLoader {

  private final Map<String, Texture> textureCache = new HashMap<>();

  public Sprite createSprite(final String path) {
    return new Sprite(textureCache.computeIfAbsent(path, this::getTexture));
  }

  @VisibleForTesting
  Texture getTexture(final String path) {
    return new Texture(Gdx.files.internal(path));
  }
}
