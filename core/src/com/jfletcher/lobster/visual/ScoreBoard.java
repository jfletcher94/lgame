package com.jfletcher.lobster.visual;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.visual.particle.VisualElement;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

public class ScoreBoard implements VisualElement {

    private static final String TEXTURE = "textures/board.png";

    // Instance variables
    private Level level;
    private Sprite sprite;

    public void create(final SpriteLoader spriteLoader,
                       final Level level) {
        this.level = level;
        sprite = spriteLoader.createSprite(TEXTURE);
        sprite.setOriginCenter();
        sprite.setOriginBasedPosition(0f, 0f);
        sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    }

    @Override
    public void render(Batch batch) {
        sprite.draw(batch);
    }

    @Override
    public void destroy() {

    }

    @Override
    public boolean isDestroyed() {
        return false;
    }

    @Override
    public void dispose() {

    }

    @Override
    public void step(float timeStep, int velocityIterations, int positionIterations) {

    }
}
