package com.jfletcher.lobster.visual.text;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.jfletcher.lobster.level.Level;

import java.util.function.Supplier;

public class TextStaticDisplay extends TextElement {

  // Instance variables
  private Level level;
  private Alignment alignment;
  private Label label;
  private Supplier<String> text;

  // State variables
  private boolean isDestroyed = false;

  public void create(
          final Level level,
          final Alignment alignment,
          final float x,
          final float y,
          final Label.LabelStyle labelStyle,
          final Supplier<String> text) {
    this.level = level;
    this.alignment = alignment;
    this.text = text;
    label = new Label("", labelStyle);
    setUp(x, y, alignment);
  }

  @Override
  public Label getLabel() {
    return label;
  }

  @Override
  public Actor getActor() {
    return label;
  }

  @Override
  public Alignment getAlignment() {
    return alignment;
  }

  @Override
  public void setAlignment(Alignment alignment) {
    this.alignment = alignment;
  }

  @Override
  protected String getText() {
    return text.get();
  }

  @Override
  public void destroy() {
    if (isDestroyed()) {
      return;
    }

    isDestroyed = true;
    level.destroy(this);
  }

  @Override
  public boolean isDestroyed() {
    return isDestroyed;
  }

  @Override
  public void dispose() {}
}
