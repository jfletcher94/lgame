package com.jfletcher.lobster.visual.text;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.jfletcher.lobster.visual.StyleManager;
import com.jfletcher.lobster.visual.particle.VisualElement;

public abstract class TextElement implements VisualElement {

  public enum Alignment {
    LEFT(Align.left),
    CENTER(Align.center),
    RIGHT(Align.right),
    TOP(Align.top),
    BOTTOM(Align.bottom),
    TOP_LEFT(Align.topLeft),
    TOP_RIGHT(Align.topRight),
    BOTTOM_LEFT(Align.bottomLeft),
    BOTTOM_RIGHT(Align.bottomRight),
    ;

    public final int align;

    Alignment(final int align) {
      this.align = align;
    }
  }

  public abstract Label getLabel();

  public abstract Actor getActor();

  public abstract Alignment getAlignment();

  public abstract void setAlignment(Alignment alignment);

  protected abstract String getText();

  protected void setUp(final float x, final float y, final Alignment alignment) {
    getLabel().setAlignment(alignment.align);
    getLabel().setOrigin(alignment.align);
    final Vector2 position = StyleManager.worldToStage(new Vector2(x, y));
    getActor().setPosition(position.x, position.y);
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    getLabel().setText(getText());
  }

  @Override
  public final void render(Batch batch) {}
}
