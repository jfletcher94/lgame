package com.jfletcher.lobster.visual.text;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.visual.StyleManager;

import java.util.function.Supplier;

public class InGameScoreDisplay {

  private static final float PADDING = 0.1725f;

  private final TextStaticDisplay bestKill;
  private final TextStaticDisplay score;
  private final TextStaticDisplay kills;

  public InGameScoreDisplay(
      final Level level,
      final Supplier<String> bestKillText,
      final Supplier<String> scoreText,
      final Supplier<String> killsText) {
    final Label.LabelStyle labelStyle =
        StyleManager.createLabelStyle(24, com.badlogic.gdx.graphics.Color.BLACK, 3, Color.WHITE);
    bestKill = new TextStaticDisplay();
    bestKill.create(
        level,
        TextElement.Alignment.BOTTOM_LEFT,
        -Constants.Scene.HALF_SIZE.x + PADDING,
        -Constants.Scene.HALF_SIZE.y + PADDING,
        labelStyle,
        bestKillText);
    level.addTextElement(bestKill);
    level.getStage().addActor(bestKill.getActor());
    score = new TextStaticDisplay();
    score.create(
        level,
        TextElement.Alignment.BOTTOM,
        0f,
        -Constants.Scene.HALF_SIZE.y + PADDING,
        labelStyle,
        scoreText);
    level.addTextElement(score);
    kills = new TextStaticDisplay();
    kills.create(
        level,
        TextElement.Alignment.BOTTOM_RIGHT,
        Constants.Scene.HALF_SIZE.x - PADDING,
        -Constants.Scene.HALF_SIZE.y + PADDING,
        labelStyle,
        killsText);
    level.addTextElement(kills);
    level.getStage().addActor(kills.getActor());
  }
}
