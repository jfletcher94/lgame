package com.jfletcher.lobster.visual;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.jfletcher.lobster.util.Constants;

public class StyleManager implements Constants {

  private static final String FONT_FILE = "fonts/04B_30.ttf";

  public static BitmapFont createFont(
      final int size, final Color color, final int borderWidth, final Color borderColor) {
    final FreeTypeFontGenerator fontGenerator =
        new FreeTypeFontGenerator(Gdx.files.internal(FONT_FILE));
    final FreeTypeFontGenerator.FreeTypeFontParameter fontParameter =
        new FreeTypeFontGenerator.FreeTypeFontParameter();
    fontParameter.size = size;
    fontParameter.color = color;
    fontParameter.borderWidth = borderWidth;
    fontParameter.borderColor = borderColor;
    fontParameter.minFilter = Texture.TextureFilter.Linear;
    fontParameter.magFilter = Texture.TextureFilter.Linear;
    final BitmapFont font = fontGenerator.generateFont(fontParameter);
    font.setFixedWidthGlyphs("0123456789");
    return font;
  }

  public static Label.LabelStyle createLabelStyle(final BitmapFont font) {
    final Label.LabelStyle labelStyle = new Label.LabelStyle();
    labelStyle.font = font;
    return labelStyle;
  }

  public static Label.LabelStyle createLabelStyle(
      final int size, final Color color, final int borderWidth, final Color borderColor) {
    return createLabelStyle(createFont(size, color, borderWidth, borderColor));
  }

  public static Vector2 worldToStage(final Vector2 vector2) {
    return (vector2.add(Scene.HALF_SIZE)).scl(1f / Scene.WORLD_TO_SCREEN);
  }

  public static Vector2 stageToWorld(final Vector2 vector2) {
    return vector2.cpy().scl(Scene.WORLD_TO_SCREEN).sub(Scene.HALF_SIZE);
  }

  public static TextField.TextFieldStyle createTextFieldStyle(
      final int size, final Color color, final int borderWidth, final Color borderColor) {
    final TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle();
    textFieldStyle.font = createFont(size, color, borderWidth, borderColor);
    textFieldStyle.fontColor = textFieldStyle.font.getColor();
    return textFieldStyle;
  }
}
