package com.jfletcher.lobster.visual.particle;

import com.badlogic.gdx.utils.Disposable;
import com.jfletcher.lobster.Renderable;
import com.jfletcher.lobster.physical.Steppable;

public interface VisualElement extends Renderable {
}
