package com.jfletcher.lobster.visual.particle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

public class TrailParticle implements VisualElement {

  public static void createParticles(
      final SpriteLoader spriteLoader,
      final Level level,
      final Body body,
      final boolean highSpeed,
      final boolean front) {
    // FIXME magic numbers
    int numParticles;
    final int speeed = (int) body.getLinearVelocity().len();
    if (speeed < 2) {
      numParticles = level.getSteps() % 7 == 0 ? 1 : 0;
    } else if (speeed < 4) {
      numParticles = level.getSteps() % 6 == 0 ? 1 : 0;
    } else if (speeed < 6) {
      numParticles = level.getSteps() % 5 == 0 ? 1 : 0;
    } else if (speeed < 8) {
      numParticles = level.getSteps() % 4 == 0 ? 1 : 0;
    } else if (speeed < 10) {
      numParticles = level.getSteps() % 3 == 0 ? 1 : 0;
    } else if (speeed < 12) {
      numParticles = level.getSteps() % 2 == 0 ? 1 : 0;
    } else {
      numParticles = 1;
    }
    if (highSpeed) {
      numParticles *= 2 + RandomUtil.intRange(0, 2);
    }
    for (int i = 0; i < numParticles; i++) {
      final TrailParticle trailParticle = new TrailParticle();
      final boolean largeParticle = highSpeed && RandomUtil.intRange(0, 3) == 0;
      trailParticle.create(
          spriteLoader,
          level,
          body.getPosition().x + RandomUtil.floatRange(-0.1f, 0.1f),
          body.getPosition().y + RandomUtil.floatRange(-0.1f, 0.1f),
          largeParticle);
      if (front) {
        level.addVisualElementFront(trailParticle);
      } else {
        level.addVisualElementBack(trailParticle);
      }
    }
  }

  private static final String SMALL_BUBBLE = "textures/small_bubble.png";
  private static final String TINY_BUBBLE = "textures/tiny_bubble.png";
  private static final float WIGGLE_SCALAR = 0.01f;
  private static final float WIGGLE_SPEED = 0.05f;
  private static final float LIFETIME_STEPS = 1.5f * Constants.Step.STEPS_PER_SECOND;

  // Instance variables
  private Level level;
  private Sprite sprite;

  // State variables
  private boolean isDestroyed = false;
  private double phaseX;
  private double phaseY;
  private float wiggleScalar;
  private float wiggleSpeed;
  private int lifetimeSteps;
  private int remainingSteps;

  public void create(
      final SpriteLoader spriteLoader,
      final Level level,
      final float x,
      final float y,
      final boolean largeParticle) {
    // FIXME magic numbers
    this.level = level;
    sprite = createSprite(spriteLoader, x, y, largeParticle);
    phaseX = RandomUtil.floatRange(0, (float) (2 * Math.PI));
    phaseY = RandomUtil.floatRange(0, (float) (2 * Math.PI));
    wiggleScalar = WIGGLE_SCALAR + RandomUtil.floatRange(-0.005f, 0.005f);
    wiggleSpeed = WIGGLE_SPEED + RandomUtil.floatRange(-0.025f, 0.025f);
    remainingSteps = lifetimeSteps = (int) (LIFETIME_STEPS * RandomUtil.floatRange(0.8f, 1.2f));
  }

  @VisibleForTesting
  Sprite createSprite(
      final SpriteLoader spriteLoader, final float x, final float y, boolean largeParticle) {
    final Sprite sprite;
    if (largeParticle) {
      sprite = spriteLoader.createSprite(SMALL_BUBBLE);
    } else {
      sprite = spriteLoader.createSprite(TINY_BUBBLE);
    }
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setOriginBasedPosition(x, y);
    // Set random phase for wiggle
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    return sprite;
  }

  @Override
  public void render(Batch batch) {
    sprite.draw(batch);
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    if (--remainingSteps <= 0) {
      destroy();
      return;
    }

    final Color color = sprite.getColor();
    sprite.setColor(color.r, color.g, color.b, ((float) remainingSteps) / ((float) lifetimeSteps));
    final float newX = sprite.getX() + wiggleScalar * (float) Math.cos(phaseX += wiggleSpeed);
    final float newY = sprite.getY() + wiggleScalar * (float) Math.cos(phaseY += wiggleSpeed);
    sprite.setPosition(newX, newY);

    //    final Vector2 position =
    //        new Vector2(sprite.getX() + sprite.getOriginX(), sprite.getY() + sprite.getOriginY());
    //    if (position.x > Constants.Scene.HALF_SIZE.x + OFFSCREEN_OFFSET
    //        || position.x < Constants.Scene.HALF_SIZE.x - OFFSCREEN_OFFSET
    //        || position.y > Constants.Scene.HALF_SIZE.y + OFFSCREEN_OFFSET
    //        || position.y < Constants.Scene.HALF_SIZE.y + OFFSCREEN_OFFSET) {
    //      destroy();
    //    }
  }

  public void setColor(final Color color) {
    sprite.setColor(color);
  }

  @Override
  public void destroy() {
    if (isDestroyed()) {
      return;
    }

    isDestroyed = true;
    level.destroy(this);
  }

  @Override
  public boolean isDestroyed() {
    return isDestroyed;
  }

  @Override
  public void dispose() {}
}
