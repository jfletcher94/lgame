package com.jfletcher.lobster.visual.particle;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

import java.util.ArrayList;
import java.util.List;

public class BodyPartParticle implements VisualElement {

  public static List<BodyPartParticle> create(
      final SpriteLoader spriteLoader,
      final Level level,
      final Vector2 position,
      final Vector2 velocity) {
    final List<BodyPartParticle> list = new ArrayList<>(17);
    for (int i = 0; i < 2; i++) {
      list.add(createHelper(spriteLoader, Textures.ARM, level, position, velocity));
    }
    list.add(createHelper(spriteLoader, Textures.BODY_0, level, position, velocity));
    list.add(createHelper(spriteLoader, Textures.BODY_1, level, position, velocity));
    list.add(createHelper(spriteLoader, Textures.BODY_2, level, position, velocity));
    list.add(createHelper(spriteLoader, Textures.BODY_3, level, position, velocity));
    for (int i = 0; i < 2; i++) {
      list.add(createHelper(spriteLoader, Textures.CLAW, level, position, velocity));
    }
    for (int i = 0; i < 2; i++) {
      list.add(createHelper(spriteLoader, Textures.EYE_STALK, level, position, velocity));
    }
    for (int i = 0; i < 6; i++) {
      list.add(createHelper(spriteLoader, Textures.LEG, level, position, velocity));
    }
    list.add(createHelper(spriteLoader, Textures.TAIL, level, position, velocity));
    return list;
  }

  private static BodyPartParticle createHelper(
      final SpriteLoader spriteLoader,
      final String spritePath,
      final Level level,
      final Vector2 position,
      final Vector2 velocity) {
    final BodyPartParticle bodyPartParticle = new BodyPartParticle();
    bodyPartParticle.create(spriteLoader, spritePath, level, position, velocity);
    return bodyPartParticle;
  }

  private interface Textures {

    String ARM = "textures/lobster_body/arm.png";
    String BODY_0 = "textures/lobster_body/body_0.png";
    String BODY_1 = "textures/lobster_body/body_1.png";
    String BODY_2 = "textures/lobster_body/body_2.png";
    String BODY_3 = "textures/lobster_body/body_3.png";
    String CLAW = "textures/lobster_body/claw.png";
    String EYE_STALK = "textures/lobster_body/eye_stalk.png";
    String LEG = "textures/lobster_body/leg.png";
    String TAIL = "textures/lobster_body/tail.png";
  }

  private static final float ROTATION_SPEED_RANGE = 20f;
  private static final float VELOCITY_RANGE = 0.005f;
  private static final float MIN_SPEED_SCALAR = 0.01f;
  private static final float GRAVITY = -0.2f / Constants.Step.STEPS_PER_SECOND;
  private static final int MAX_BOUNCES = 1;

  // Instance variables
  private Level level;
  private Sprite sprite;

  // State variables
  private boolean isDestroyed = false;
  private float rotationSpeed;
  private Vector2 velocity;
  private int xBounces;
  private int yBounces;

  public void create(
      final SpriteLoader spriteLoader,
      final String spritePath,
      final Level level,
      final Vector2 position,
      final Vector2 velocity) {
    this.level = level;
    sprite = createSprite(spriteLoader, spritePath, position.x, position.y);
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    sprite.setOriginCenter();
    sprite.setRotation(RandomUtil.floatRange(0, (float) (2 * Math.PI)));
    xBounces = MAX_BOUNCES;
    yBounces = MAX_BOUNCES;
    rotationSpeed = RandomUtil.floatRange(-ROTATION_SPEED_RANGE, ROTATION_SPEED_RANGE);
    final float speedScalar = MIN_SPEED_SCALAR + velocity.len();
    this.velocity =
        new Vector2(
            velocity.x * Constants.Step.TIME_STEP
                + speedScalar * RandomUtil.floatRange(-VELOCITY_RANGE, VELOCITY_RANGE),
            velocity.y * Constants.Step.TIME_STEP
                + speedScalar * RandomUtil.floatRange(-VELOCITY_RANGE, VELOCITY_RANGE));
  }

  @VisibleForTesting
  Sprite createSprite(
      final SpriteLoader spriteLoader, final String spritePath, final float x, final float y) {
    final Sprite sprite = spriteLoader.createSprite(spritePath);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setOriginBasedPosition(x, y);
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN * 2f);
    return sprite;
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    bounce();
    final float newX = sprite.getX() + velocity.x;
    final float newY = sprite.getY() + (velocity.y += GRAVITY);
    sprite.setPosition(newX, newY);
    sprite.rotate(rotationSpeed);
  }

  private void bounce() {
    final float spriteX = sprite.getX() + sprite.getOriginX();
    final float spriteY = sprite.getY() + sprite.getOriginY();
    if (spriteX < -Constants.Scene.HALF_SIZE.x || spriteX > Constants.Scene.HALF_SIZE.x) {
      if (xBounces-- <= 0) {
        destroy();
        return;
      }

      velocity.x *= -0.95;
    }
    if (spriteY < -Constants.Scene.HALF_SIZE.y) {
      if (yBounces-- <= 0) {
        destroy();
        return;
      }

      velocity.y *= -0.95;
    } else if (spriteY > Constants.Scene.HALF_SIZE.y) {
      velocity.y *= -0.95;
    }
  }

  @Override
  public void render(Batch batch) {
    sprite.draw(batch);
  }

  @Override
  public void destroy() {
    if (isDestroyed()) {
      return;
    }

    isDestroyed = true;
    level.destroy(this);
  }

  @Override
  public boolean isDestroyed() {
    return isDestroyed;
  }

  @Override
  public void dispose() {}
}
