package com.jfletcher.lobster.visual.particle;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.Logger;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

public class Bubble implements VisualElement {

  private static final Logger logger = new Logger(Bubble.class);

  public static final float HALF_SPRITE_HEIGHT = 10f;
  private static final String BUBBLE = "textures/bubble.png";
  private static final String LARGE_BUBBLE = "textures/large_bubble.png";
  private static final float RISE_SPEED = 0.075f;
  private static final float WIGGLE_SCALAR = 0.02f;
  private static final float WIGGLE_SPEED = 0.1f;

  // Instance variables
  private Level level;
  private Sprite sprite;

  // State variables
  private boolean isDestroyed = false;
  private double phase;
  private float riseSpeed;
  private float wiggleScalar;
  private float wiggleSpeed;

  public void create(
      final SpriteLoader spriteLoader,
      final Level level,
      final float x,
      final float y,
      final boolean largeBubble) {
    // FIXME magic numbers
    this.level = level;
    sprite = createSprite(spriteLoader, x, y, largeBubble);
    phase = RandomUtil.floatRange(0, (float) (2 * Math.PI));
    riseSpeed = RISE_SPEED + RandomUtil.floatRange(-0.015f, 0.015f);
    wiggleScalar = WIGGLE_SCALAR + RandomUtil.floatRange(-0.01f, 0.01f);
    wiggleSpeed = WIGGLE_SPEED + RandomUtil.floatRange(-0.05f, 0.05f);
  }

  @VisibleForTesting
  Sprite createSprite(
      final SpriteLoader spriteLoader, final float x, final float y, final boolean largeBubble) {
    final Sprite sprite =
        largeBubble ? spriteLoader.createSprite(LARGE_BUBBLE) : spriteLoader.createSprite(BUBBLE);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setOriginBasedPosition(x, y);
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    return sprite;
  }

  @Override
  public void render(Batch batch) {
    sprite.draw(batch);
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    final float newX = sprite.getX() + wiggleScalar * (float) Math.cos(phase += wiggleSpeed);
    final float newY = sprite.getY() + riseSpeed;
    sprite.setPosition(newX, newY);
    if (sprite.getY() + sprite.getOriginY()
        > Constants.Scene.HALF_SIZE.y + Constants.Scene.WORLD_TO_SCREEN * HALF_SPRITE_HEIGHT) {
      destroy();
    }
  }

  @Override
  public void destroy() {
    if (isDestroyed()) {
      return;
    }

    isDestroyed = true;
    level.destroy(this);
  }

  @Override
  public boolean isDestroyed() {
    return isDestroyed;
  }

  @Override
  public void dispose() {}
}
