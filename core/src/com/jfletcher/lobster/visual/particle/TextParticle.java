package com.jfletcher.lobster.visual.particle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.StyleManager;
import com.jfletcher.lobster.visual.text.TextElement;

import java.util.function.Supplier;

public class TextParticle extends TextElement {

  private static final float LIFETIME_STEPS = 1f * Constants.Step.STEPS_PER_SECOND;

  // Instance variables
  private Level level;
  private Alignment alignment;
  private Supplier<String> text;
  private Label label;
  private Container<Label> container;

  // State variables
  private boolean isDestroyed = false;
  private int lifetimeSteps;
  private int remainingSteps;

  private static float direction = RandomUtil.oneIn(2) ? 1 : -1;

  public void create(
      final Level level,
      final Alignment alignment,
      final float x,
      final float y,
      final Supplier<String> text,
      final int fontSize,
      final Color color) {
    create(level, alignment, x, y, text, fontSize, color, Color.WHITE);
  }

  public void create(
      final Level level,
      final Alignment alignment,
      final float x,
      final float y,
      final Supplier<String> text,
      final int fontSize,
      final Color color,
      final Color outlineColor) {
    this.level = level;
    this.alignment = alignment;
    this.text = text;
    remainingSteps = (int) LIFETIME_STEPS;
    lifetimeSteps = remainingSteps / 2;
    label = new Label("", StyleManager.createLabelStyle(fontSize, color, 2, outlineColor));
    container = new Container<>(label);
    setUp(x, y, alignment);
    container.setTransform(true);
    container.setRotation((direction *= -1) * RandomUtil.floatRange(0, 20)); // TEMP
  }

  @Override
  public Actor getActor() {
    return container;
  }

  @Override
  public Label getLabel() {
    return label;
  }

  @Override
  public Alignment getAlignment() {
    return alignment;
  }

  @Override
  public void setAlignment(final Alignment alignment) {
    this.alignment = alignment;
  }

  @Override
  protected String getText() {
    return text.get();
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    super.step(timeStep, velocityIterations, positionIterations);
    if (--remainingSteps <= 0) {
      destroy();
      return;
    }

    if (remainingSteps < lifetimeSteps) {
      getActor().getColor().a = ((float) remainingSteps) / ((float) lifetimeSteps);
    }
  }

  @Override
  public void destroy() {
    if (isDestroyed()) {
      return;
    }

    isDestroyed = true;
    level.destroy(this);
  }

  @Override
  public boolean isDestroyed() {
    return isDestroyed;
  }

  @Override
  public void dispose() {}
}
