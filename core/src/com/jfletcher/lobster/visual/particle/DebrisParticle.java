package com.jfletcher.lobster.visual.particle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

public class DebrisParticle implements VisualElement {

  public static final float HALF_SPRITE_HEIGHT = 4f;
  private static final String SMALL_DEBRIS = "textures/debris_s.png";
  private static final String MEDIUM_DEBRIS_1 = "textures/debris_m_0.png";
  private static final String MEDIUM_DEBRIS_2 = "textures/debris_m_1.png";
  private static final float ROTATION_SPEED_RANGE = 20f;
  private static final float VELOCITY_RANGE = 0.1f;
  private static final float GRAVITY = -0.2f / Constants.Step.STEPS_PER_SECOND;

  // Instance variables
  private Level level;
  private Sprite sprite;

  // State variables
  private boolean isDestroyed = false;
  private float rotationSpeed;
  private Vector2 velocity;

  public void create(
      final SpriteLoader spriteLoader,
      final Level level,
      final float x,
      final float y,
      final Color color) {
    this.level = level;
    sprite = createSprite(spriteLoader, x, y, color);
    sprite.setOriginCenter();
    sprite.setRotation(RandomUtil.floatRange(0, (float) (2 * Math.PI)));
    rotationSpeed = RandomUtil.floatRange(-ROTATION_SPEED_RANGE, ROTATION_SPEED_RANGE);
    velocity =
        new Vector2(
            RandomUtil.floatRange(-VELOCITY_RANGE, VELOCITY_RANGE),
            RandomUtil.floatRange(-VELOCITY_RANGE, VELOCITY_RANGE));
  }

  public void create(
      final SpriteLoader spriteLoader,
      final Level level,
      final float x,
      final float y,
      final String texture) {
    this.level = level;
    sprite = createSprite(spriteLoader, x, y, texture);
    sprite.setOriginCenter();
    sprite.setRotation(RandomUtil.floatRange(0, (float) (2 * Math.PI)));
    rotationSpeed = RandomUtil.floatRange(-ROTATION_SPEED_RANGE, ROTATION_SPEED_RANGE);
    velocity =
            new Vector2(
                    RandomUtil.floatRange(-VELOCITY_RANGE, VELOCITY_RANGE),
                    RandomUtil.floatRange(-VELOCITY_RANGE, VELOCITY_RANGE));
  }

  @VisibleForTesting
  Sprite createSprite(
          final SpriteLoader spriteLoader, final float x, final float y, final Color color) {
    final Sprite sprite;
    switch (RandomUtil.intRange(0, 4)) {
      case 0:
        sprite = spriteLoader.createSprite(MEDIUM_DEBRIS_1);
        break;
      case 1:
        sprite = spriteLoader.createSprite(MEDIUM_DEBRIS_2);
        break;
      default:
        sprite = spriteLoader.createSprite(SMALL_DEBRIS);
        break;
    }
    sprite.setColor(color);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setOriginBasedPosition(x, y);
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN * 1.5f);
    return sprite;
  }

  @VisibleForTesting
  Sprite createSprite(
          final SpriteLoader spriteLoader, final float x, final float y, final String texture) {
    final Sprite sprite;
    sprite = spriteLoader.createSprite(texture);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setOriginBasedPosition(x, y);
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    return sprite;
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    final float newX = sprite.getX() + velocity.x;
    final float newY = sprite.getY() + (velocity.y += GRAVITY);
    sprite.setPosition(newX, newY);
    sprite.rotate(rotationSpeed);
    if (sprite.getY() + sprite.getOriginY()
        < -Constants.Scene.HALF_SIZE.y - Constants.Scene.WORLD_TO_SCREEN * HALF_SPRITE_HEIGHT) {
      destroy();
    }
  }

  @Override
  public void render(Batch batch) {
    sprite.draw(batch);
  }

  @Override
  public void destroy() {
    if (isDestroyed()) {
      return;
    }

    isDestroyed = true;
    level.destroy(this);
  }

  @Override
  public boolean isDestroyed() {
    return isDestroyed;
  }

  @Override
  public void dispose() {}
}
