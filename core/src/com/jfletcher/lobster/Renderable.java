package com.jfletcher.lobster;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Disposable;
import com.jfletcher.lobster.physical.Steppable;

public interface Renderable  extends Steppable, Disposable {

    void render(final Batch batch);

    void destroy();

    boolean isDestroyed();
}
