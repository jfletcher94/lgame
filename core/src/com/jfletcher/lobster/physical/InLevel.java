package com.jfletcher.lobster.physical;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Disposable;
import com.jfletcher.lobster.Renderable;

public interface InLevel extends  Renderable {

  Body getBody();
}
