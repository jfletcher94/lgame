package com.jfletcher.lobster.physical;

public interface Steppable {

  void step(final float timeStep, final int velocityIterations, final int positionIterations);
}
