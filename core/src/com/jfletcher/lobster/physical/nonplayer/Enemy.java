package com.jfletcher.lobster.physical.nonplayer;

import com.jfletcher.lobster.physical.contact.Contactable;

public interface Enemy extends Contactable {

  default int getDamage() {
    return 1;
  }
}
