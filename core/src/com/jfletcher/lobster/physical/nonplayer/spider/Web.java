package com.jfletcher.lobster.physical.nonplayer.spider;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Suppliers;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.contact.Contactable;
import com.jfletcher.lobster.physical.contact.LobsterContact;
import com.jfletcher.lobster.physical.player.lobster.Lobster;
import com.jfletcher.lobster.util.Constants;

import java.util.function.Supplier;

public class Web implements Contactable {

  private interface Const {

    interface Texture {
      String WHITE = "textures/white.png";
    }

    float WIDTH = 1.5f * Constants.Scene.WORLD_TO_SCREEN;
  }

  private static final Supplier<Texture> texture =
      Suppliers.memoize(() -> new Texture(Gdx.files.internal(Const.Texture.WHITE)));

  // Instance variables
  private Level level;
  private Spider spider;
  private Body body;
  private Sprite sprite;

  // State variables
  private boolean isDestroyed = false;

  public void create(final Level level, final Spider spider) {
    this.level = level;
    this.spider = spider;
    body = createBody();
  }

  @VisibleForTesting
  Body createBody() {
    // TODO use SpriteLoader
    sprite = new Sprite(new TextureRegion(texture.get()));
    final BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyDef.BodyType.StaticBody;
    body = level.getWorld().createBody(bodyDef);
    body.setUserData(this);
    return body;
  }

  public Spider getSpider() {
    return spider;
  }

  @Override
  public void step(
      final float timeStep, final int velocityIterations, final int positionIterations) {
    body.getFixtureList().forEach(body::destroyFixture);
    final EdgeShape edgeShape = new EdgeShape();
    edgeShape.set(
        spider.getPosition(), new Vector2(spider.getPosition().x, Constants.Scene.HALF_SIZE.y));
    final FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = edgeShape;
    body.createFixture(fixtureDef);
    edgeShape.dispose();
  }

  @Override
  public void contact(final LobsterContact lobsterContact, final Lobster lobster) {
    lobster.contactWeb(lobsterContact, this);
  }

  @Override
  public Body getBody() {
    return body;
  }

  @Override
  public void destroy() {
    if (isDestroyed) {
      return;
    }
    isDestroyed = true;
    spider.destroy();
    level.destroy(this);
  }

  @Override
  public boolean isDestroyed() {
    return isDestroyed;
  }

  @Override
  public void render(final Batch batch) {
    sprite.setPosition(spider.getPosition().x, spider.getPosition().y);
    sprite.setSize(Const.WIDTH, Constants.Scene.HALF_SIZE.y - spider.getPosition().y);
    sprite.draw(batch);
  }

  @Override
  public void dispose() {
  }
}
