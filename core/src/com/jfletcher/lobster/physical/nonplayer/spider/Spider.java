package com.jfletcher.lobster.physical.nonplayer.spider;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.contact.LobsterContact;
import com.jfletcher.lobster.physical.environment.friendly.powerup.Slow;
import com.jfletcher.lobster.physical.nonplayer.Enemy;
import com.jfletcher.lobster.physical.player.lobster.Lobster;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.particle.Bubble;
import com.jfletcher.lobster.visual.particle.DebrisParticle;
import com.jfletcher.lobster.visual.particle.TrailParticle;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

public class Spider implements Enemy {

  public interface Const {

    interface Body {
      float RADIUS = 0.15f;
    }

    interface Movement {
      float TOP_BOUNCE_SPEED_SCALAR = -2.0f;
      float BOTTOM_BOUNCE_SPEED_SCALAR_ACCELERATING = 1.05f / TOP_BOUNCE_SPEED_SCALAR;
      float BOTTOM_BOUNCE_SPEED_SCALAR_CONSTANT = 1.0f / TOP_BOUNCE_SPEED_SCALAR;
      float MAX_NEGATIVE_SPEED = -10.0f;
    }

    interface Texture {
      String SPIDER = "textures/spider0_a.png";
      String SLOWNESS = "textures/slowness.png";
      String EYE = "textures/spider_eye.png";
      int REGION_WIDTH = 54;
      int REGION_HEIGHT = 54;
      int REGION_COUNT = 2;
      int SPRITE_ANIMATION_SCALE = 36;
    }

    interface Visuals {
      int SPLASHDOWN_FRAMES = 1 * Constants.Step.STEPS_PER_SECOND;
      Color COLOR = Color.BLACK;
    }
  }

  // Instance variables
  private Level level;
  private Sprite spiderSprite;
  private Sprite slownessSprite;
  private Body body;
  private Web web;
  private SpriteLoader spriteLoader;

  // State variables
  private boolean isDestroyed = false;
  private boolean isSlowed = false;
  private int spriteRegion;
  private int spriteAnimationScale = Const.Texture.SPRITE_ANIMATION_SCALE;
  private int splashdownFrames = Const.Visuals.SPLASHDOWN_FRAMES;

  public void create(
      final SpriteLoader spriteLoader,
      final Level level,
      final Vector2 position,
      final Vector2 velocity) {
    this.spriteLoader = spriteLoader;
    this.level = level;
    spiderSprite = createSpiderSprite();
    slownessSprite = createSlownessSprite();
    body = createBody(position, velocity);
    web = createWeb();
  }

  @VisibleForTesting
  Sprite createSpiderSprite() {
    final Sprite sprite = spriteLoader.createSprite(Const.Texture.SPIDER);
    sprite.setRegion(0, 0, Const.Texture.REGION_WIDTH, Const.Texture.REGION_HEIGHT);
    sprite.setSize(Const.Texture.REGION_WIDTH, Const.Texture.REGION_HEIGHT);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    spriteRegion = RandomUtil.intRange(0, Const.Texture.REGION_COUNT);
    sprite.setRegion(
        Const.Texture.REGION_WIDTH * spriteRegion,
        0,
        Const.Texture.REGION_WIDTH,
        Const.Texture.REGION_HEIGHT);
    return sprite;
  }

  @VisibleForTesting
  private Sprite createSlownessSprite() {
    final Sprite sprite = spriteLoader.createSprite(Const.Texture.SLOWNESS);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    return sprite;
  }

  @VisibleForTesting
  Body createBody(final Vector2 position, final Vector2 velocity) {
    final BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyDef.BodyType.KinematicBody;
    bodyDef.position.set(position);
    bodyDef.linearVelocity.set(velocity);
    final CircleShape circleShape = new CircleShape();
    circleShape.setRadius(Const.Body.RADIUS);
    final FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = circleShape;
    final Body body = level.getWorld().createBody(bodyDef);
    body.createFixture(fixtureDef);
    body.setUserData(this);
    circleShape.dispose();
    return body;
  }

  @VisibleForTesting
  Web createWeb() {
    final Web web = new Web();
    web.create(level, this);
    return web;
  }

  public void slow(final float scalar) {
    getBody().setLinearVelocity(getBody().getLinearVelocity().scl(scalar));
    createSlimeParticles();
    if (isSlowed) {
      return;
    }

    isSlowed = true;
    spriteAnimationScale *= 2;
  }

  @VisibleForTesting
  void cycleSpriteRegion() {
    if (level.getSteps() % spriteAnimationScale != 0) {
      return;
    }

    if (++spriteRegion >= Const.Texture.REGION_COUNT) {
      spriteRegion = 0;
    }
    spiderSprite.setRegion(
        Const.Texture.REGION_WIDTH * spriteRegion,
        0,
        Const.Texture.REGION_WIDTH,
        Const.Texture.REGION_HEIGHT);
  }

  @Override
  public void step(
      final float timeStep, final int velocityIterations, final int positionIterations) {
    cycleSpriteRegion();
    bounce();
    splashdown();
    web.step(timeStep, velocityIterations, positionIterations);
  }

  @Override
  public void contact(final LobsterContact lobsterContact, final Lobster lobster) {
    lobster.contactSpider(lobsterContact, this);
  }

  @VisibleForTesting
  void bounce() {
    if (body.getPosition().y > Constants.Scene.HALF_SIZE.y) {
      body.setLinearVelocity(
          body.getLinearVelocity().x,
          body.getLinearVelocity().y * Const.Movement.TOP_BOUNCE_SPEED_SCALAR);
      body.setTransform(body.getPosition().x, Constants.Scene.HALF_SIZE.y, 0.0f);
    } else if (body.getPosition().y < -Constants.Scene.HALF_SIZE.y) {
      if (body.getLinearVelocity().y < Const.Movement.MAX_NEGATIVE_SPEED) {
        body.setLinearVelocity(
            body.getLinearVelocity().x,
            body.getLinearVelocity().y * Const.Movement.BOTTOM_BOUNCE_SPEED_SCALAR_CONSTANT);
      } else {
        body.setLinearVelocity(
            body.getLinearVelocity().x,
            body.getLinearVelocity().y * Const.Movement.BOTTOM_BOUNCE_SPEED_SCALAR_ACCELERATING);
      }
      body.setTransform(body.getPosition().x, -Constants.Scene.HALF_SIZE.y, 0.0f);
    }
  }

  @Override
  public Body getBody() {
    return body;
  }

  @Override
  public void destroy() {
    if (isDestroyed()) {
      return;
    }

    isDestroyed = true;
    web.destroy();
    level.destroy(this);
  }

  @Override
  public boolean isDestroyed() {
    return isDestroyed;
  }

  @Override
  public void render(final Batch batch) {
    web.render(batch);
    spiderSprite.setOriginBasedPosition(body.getPosition().x, body.getPosition().y);
    spiderSprite.draw(batch);
    if (isSlowed) {
      slownessSprite.setOriginBasedPosition(body.getPosition().x, body.getPosition().y);
      slownessSprite.draw(batch);
    }
  }

  @Override
  public void dispose() {}

  public Vector2 getPosition() {
    return body.getPosition();
  }

  public void addDeathVisuals() {
    for (int i = 0; i < 8; i++) {
      final DebrisParticle debrisParticle = new DebrisParticle();
      debrisParticle.create(
              spriteLoader,
              level,
              getBody().getPosition().x,
              getBody().getPosition().y,
              Const.Visuals.COLOR);
      level.addVisualElementFront(debrisParticle);
    }
    for (int i = 0; i < RandomUtil.intRange(1, 5); i++) {
      final DebrisParticle debrisParticle = new DebrisParticle();
      debrisParticle.create(
              spriteLoader,
              level,
              getBody().getPosition().x,
              getBody().getPosition().y,
              Const.Texture.EYE);
      level.addVisualElementFront(debrisParticle);
    }
  }

  private void splashdown() {
    if (splashdownFrames <= 0) {
      return;
    }

    splashdownFrames--;
    if (splashdownFrames < Const.Visuals.SPLASHDOWN_FRAMES / 2 && level.getSteps() % 2 == 0
        || splashdownFrames < Const.Visuals.SPLASHDOWN_FRAMES / 4 && level.getSteps() % 4 == 0
        || RandomUtil.oneIn(2)) {
      return;
    }

    if (splashdownFrames < 2 * Const.Visuals.SPLASHDOWN_FRAMES / 3 && RandomUtil.oneIn(10)) {
      final Bubble bubble = new Bubble();
      bubble.create(
          spriteLoader,
          level,
          getBody().getPosition().x + RandomUtil.floatRange(-0.1f, 0.1f),
          getBody().getPosition().y + RandomUtil.floatRange(-0.1f, 0.1f),
          false);
      level.addVisualElementFront(bubble);
      return;
    }

    final boolean largeParticle = RandomUtil.oneIn(2);
    final TrailParticle trailParticle = new TrailParticle();
    trailParticle.create(
        spriteLoader,
        level,
        getBody().getPosition().x + RandomUtil.floatRange(-0.1f, 0.1f),
        getBody().getPosition().y + RandomUtil.floatRange(-0.1f, 0.1f),
        largeParticle);
    level.addVisualElementFront(trailParticle);
  }

  private void createSlimeParticles() {
    for (int i = 0; i < 10; i++) {
      final DebrisParticle debrisParticle = new DebrisParticle();
      debrisParticle.create(
          spriteLoader, level, getBody().getPosition().x, getBody().getPosition().y, Slow.COLOR);
      level.addVisualElementFront(debrisParticle);
    }
  }
}
