package com.jfletcher.lobster.physical.nonplayer.hermitcrab;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.audio.SoundEffectManager;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.contact.Contactable;
import com.jfletcher.lobster.physical.contact.LobsterContact;
import com.jfletcher.lobster.physical.player.lobster.Lobster;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.particle.DebrisParticle;
import com.jfletcher.lobster.visual.particle.TrailParticle;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

public class HermitCrab implements Contactable {

  enum LaunchState {
    NONE,
    PREPARING,
    FLYING
  }

  private static final float SAND_SPAWN_OFFSET = 0.02f;
  private static final Color SAND_COLOR = new Color(0xebd7a8ff);
  private static final float RADIUS = 0.2f;
  private static final float SPEED = 0.45f;
  private static final float HALF_TEXTURE_WIDTH = 24.0f * Constants.Scene.WORLD_TO_SCREEN;
  private static final float HALF_TEXTURE_HEIGHT = 8.0f * Constants.Scene.WORLD_TO_SCREEN;
  private static final Vector2 LAUNCH_VELOCITY = new Vector2(0.0f, 8.0f);
  private static final Vector2 LEFT_POSITION =
      new Vector2(
          -Constants.Scene.HALF_SIZE.x - HALF_TEXTURE_WIDTH,
          -Constants.Scene.HALF_SIZE.y + HALF_TEXTURE_HEIGHT);
  private static final Vector2 RIGHT_POSITION =
      new Vector2(
          Constants.Scene.HALF_SIZE.x + HALF_TEXTURE_WIDTH,
          -Constants.Scene.HALF_SIZE.y + HALF_TEXTURE_HEIGHT);
  // TODO create texture
  private static final String TEXTURE = "textures/hermit_crab_0.png";
  private static final float LAUNCH_X_MIN = -Constants.Scene.HALF_SIZE.x + 0.5f;
  private static final float LAUNCH_X_MAX = Constants.Scene.HALF_SIZE.x - 0.5f;
  private static final int LAUNCH_PROBABILITY = 300;
  private static final int LAUNCH_STEP_DELAY = 1 * Constants.Step.STEPS_PER_SECOND;

  private Level level;
  private Sprite sprite;
  private Body body;
  private SharpShell sharpShell;
  private SpriteLoader spriteLoader;
  private boolean enterLeft;

  private boolean isDestroyed = false;
  private long launchStepsStart = 0L;
  private LaunchState launchState = LaunchState.NONE;

  public void create(final SpriteLoader spriteLoader, final Level level, final boolean enterLeft) {
    this.spriteLoader = spriteLoader;
    this.level = level;
    this.enterLeft = enterLeft;
    sprite = createSprite();
    body = createBody();
    sharpShell = createSharpShell();
  }

  @VisibleForTesting
  Sprite createSprite() {
    final Sprite sprite = spriteLoader.createSprite(TEXTURE);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    if (!enterLeft) {
      sprite.flip(true, false);
    }
    return sprite;
  }

  @VisibleForTesting
  Body createBody() {
    final BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyDef.BodyType.KinematicBody;
    bodyDef.position.set(enterLeft ? LEFT_POSITION : RIGHT_POSITION);
    bodyDef.linearVelocity.set(getDefaultVelocity());
    final CircleShape circleShape = new CircleShape();
    circleShape.setRadius(RADIUS);
    final FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = circleShape;
    final Body body = level.getWorld().createBody(bodyDef);
    body.createFixture(fixtureDef);
    body.setUserData(this);
    circleShape.dispose();
    return body;
  }

  @VisibleForTesting
  SharpShell createSharpShell() {
    final SharpShell sharpShell = new SharpShell();
    sharpShell.create(level, spriteLoader, this);
    return sharpShell;
  }

  @VisibleForTesting
  Vector2 getDefaultVelocity() {
    return new Vector2(enterLeft ? SPEED : -SPEED, 0.0f);
  }

  @Override
  public void contact(final LobsterContact lobsterContact, final Lobster lobster) {
    if (launchState != LaunchState.FLYING) {
      lobsterContact.getContact().setEnabled(false);
      return;
    }

    lobster.contactHermitCrab(lobsterContact, this);
  }

  @Override
  public Body getBody() {
    return body;
  }

  @Override
  public void destroy() {
    if (isDestroyed()) {
      return;
    }

    isDestroyed = true;
    sharpShell.destroy();
    level.destroy(this);
  }

  @Override
  public boolean isDestroyed() {
    return isDestroyed;
  }

  @Override
  public void render(final Batch batch) {
    sprite.setOriginBasedPosition(body.getPosition().x, body.getPosition().y);
    sprite.draw(batch);
    sharpShell.render(batch);
  }

  @Override
  public void step(
      final float timeStep, final int velocityIterations, final int positionIterations) {
    if (isOutsideLevel()) {
      destroy();
      return;
    }

    // During launch, idle
    if (launchState == LaunchState.FLYING) {
      sharpShell.step(timeStep, velocityIterations, positionIterations);
      return;
    }

    // Wait LAUNCH_STEP_DELAY steps between stopping and launching (or continuing)
    if (launchState == LaunchState.PREPARING) {
      if (level.getSteps() - launchStepsStart > LAUNCH_STEP_DELAY) {
        if (RandomUtil.oneIn(2)) {
          launch();
        } else {
          unLaunch();
        }
      }
      return;
    }

    if (level.getSteps() % 12 == 0) {
      final TrailParticle trailParticle = new TrailParticle();
      trailParticle.create(
          spriteLoader, level, getBody().getPosition().x, -Constants.Scene.HALF_SIZE.y - SAND_SPAWN_OFFSET, false);
      trailParticle.setColor(SAND_COLOR);
      level.addVisualElementFront(trailParticle);
    }
    // Possibly begin launching
    if (body.getPosition().x > LAUNCH_X_MIN
        && body.getPosition().x < LAUNCH_X_MAX
        && RandomUtil.oneIn(LAUNCH_PROBABILITY)) {
      prepare();
    }
  }

  public void addDeathVisuals() {
    for (int i = 0; i < 16; i++) {
      final DebrisParticle debrisParticle = new DebrisParticle();
      debrisParticle.create(spriteLoader, level, sharpShell.getBody().getPosition().x,
              sharpShell.getBody().getPosition().y, SharpShell.COLOR);
      level.addVisualElementFront(debrisParticle);
    }
  }

  @VisibleForTesting
  void prepare() {
    launchState = LaunchState.PREPARING;
    getBody().setLinearVelocity(Vector2.Zero);
    sharpShell.getBody().setLinearVelocity(Vector2.Zero);
    launchStepsStart = level.getSteps();
  }

  @VisibleForTesting
  void launch() {
    level.getSoundEffectManager().play(SoundEffectManager.SoundEffect.LAUNCH);
    launchState = LaunchState.FLYING;
    sharpShell.getBody().setLinearVelocity(LAUNCH_VELOCITY);
  }

  void unLaunch() {
    launchState = LaunchState.NONE;
    getBody().setLinearVelocity(getDefaultVelocity());
    sharpShell.getBody().setLinearVelocity(getDefaultVelocity());
    sharpShell.getBody().setTransform(getBody().getPosition(), sharpShell.getBody().getAngle());
  }

  @VisibleForTesting
  boolean isOutsideLevel() {
    return enterLeft
        ? getBody().getPosition().x > RIGHT_POSITION.x
        : getBody().getPosition().x < LEFT_POSITION.x;
  }

  @Override
  public void dispose() {}
}
