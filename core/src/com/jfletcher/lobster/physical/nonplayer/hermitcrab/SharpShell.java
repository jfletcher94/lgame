package com.jfletcher.lobster.physical.nonplayer.hermitcrab;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.contact.LobsterContact;
import com.jfletcher.lobster.physical.nonplayer.Enemy;
import com.jfletcher.lobster.physical.nonplayer.spider.Spider;
import com.jfletcher.lobster.physical.player.lobster.Lobster;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.particle.TrailParticle;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

public class SharpShell implements Enemy {

  public static final Color COLOR = new Color(0x016635ff);
  private static final float OFFSET = 16.0f * Constants.Scene.WORLD_TO_SCREEN;
  private static final float GRAVITY = -4.0f;
  private static final Vector2[] VERTICES =
      new Vector2[] {
        new Vector2(-0.2f, -0.15f), new Vector2(0.2f, -0.15f), new Vector2(0.0f, 0.5f)
      };
  private static final String TEXTURE = "textures/sharp_shell_0_a.png";
  private static final int REGION_WIDTH = 64;
  private static final int REGION_HEIGHT = 64;
  private static final int REGION_COUNT = 6;
  private static final int SPRITE_ANIMATION_SCALE = 3;

  private Level level;
  private Sprite sprite;
  private Body body;
  private HermitCrab hermitCrab;
  private SpriteLoader spriteLoader;

  private boolean isDestroyed = false;
  private int spriteRegion;

  public void create(
      final Level level, final SpriteLoader spriteLoader, final HermitCrab hermitCrab) {
    this.level = level;
    this.spriteLoader = spriteLoader;
    this.hermitCrab = hermitCrab;
    this.sprite = createSprite();
    this.body = createBody();
    resetSpriteRegion();
  }

  @VisibleForTesting
  Sprite createSprite() {
    final Sprite sprite = spriteLoader.createSprite(TEXTURE);
    sprite.setRegion(0, 0, Spider.Const.Texture.REGION_WIDTH, Spider.Const.Texture.REGION_HEIGHT);
    sprite.setSize(Spider.Const.Texture.REGION_WIDTH, Spider.Const.Texture.REGION_HEIGHT);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    spriteRegion = RandomUtil.intRange(0, Spider.Const.Texture.REGION_COUNT);
    return sprite;
  }

  @VisibleForTesting
  Body createBody() {
    final BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyDef.BodyType.KinematicBody;
    bodyDef.position.set(hermitCrab.getBody().getPosition());
    bodyDef.linearVelocity.set(hermitCrab.getBody().getLinearVelocity());
    final PolygonShape polygonShape = new PolygonShape();
    polygonShape.set(VERTICES);
    final FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = polygonShape;
    final Body body = level.getWorld().createBody(bodyDef);
    body.createFixture(fixtureDef);
    body.setUserData(this);
    polygonShape.dispose();
    return body;
  }

  @Override
  public void contact(final LobsterContact lobsterContact, final Lobster lobster) {
    lobster.contactSharpShell(lobsterContact, this);
  }

  @Override
  public Body getBody() {
    return body;
  }

  @Override
  public void destroy() {
    if (isDestroyed()) {
      return;
    }

    isDestroyed = true;
    hermitCrab.destroy();
    level.destroy(this);
  }

  @Override
  public boolean isDestroyed() {
    return isDestroyed;
  }

  @Override
  public void render(final Batch batch) {
    sprite.setOriginBasedPosition(getBody().getPosition().x, getBody().getPosition().y + OFFSET);
    sprite.draw(batch);
  }

  @Override
  public void step(
      final float timeStep, final int velocityIterations, final int positionIterations) {
    // Launch finished
    if (getBody().getPosition().y <= hermitCrab.getBody().getPosition().y) {
      hermitCrab.unLaunch();
      resetSpriteRegion();
      return;
    }

    cycleSpriteRegion();
    TrailParticle.createParticles(spriteLoader, level, body, body.getLinearVelocity().y > 0, true);
    getBody()
        .setLinearVelocity(
            getBody().getLinearVelocity().x,
            getBody().getLinearVelocity().y + GRAVITY * Constants.Step.TIME_STEP);
  }

  @VisibleForTesting
  void cycleSpriteRegion() {
    if (level.getSteps() % SPRITE_ANIMATION_SCALE != 0) {
      return;
    }

    if (++spriteRegion >= REGION_COUNT) {
      spriteRegion = 0;
    }
    sprite.setRegion(REGION_WIDTH * spriteRegion, 0, REGION_WIDTH, REGION_HEIGHT);
  }

  private void resetSpriteRegion() {
    sprite.setRegion(0, 0, REGION_WIDTH, REGION_HEIGHT);
  }

  @Override
  public void dispose() {}
}
