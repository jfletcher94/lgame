package com.jfletcher.lobster.physical.environment.friendly.powerup;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.contact.Contactable;
import com.jfletcher.lobster.physical.contact.LobsterContact;
import com.jfletcher.lobster.physical.player.lobster.Lobster;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

import java.util.Arrays;
import java.util.function.Supplier;

public abstract class PowerUp implements Contactable {

  public enum Type {
    FLIGHT(1, Flight::new),
    SHIELD(1, Shield::new),
    SLOW(2, Slow::new);

    public static final int TOTAL_PROBABILITY =
        Arrays.stream(Type.values()).mapToInt(Type::getProbability).sum();

    private final int probability;
    private final Supplier<PowerUp> supplier;

    Type(final int probability,
         final Supplier<PowerUp> supplier) {
      this.probability = probability;
      this.supplier = supplier;
    }

    public int getProbability() {
      return probability;
    }

    public Supplier<PowerUp> getSupplier() {
      return supplier;
    }
  }

  static final int AGE_LIMIT = 10 * Constants.Step.STEPS_PER_SECOND;
  static final float RADIUS = 0.15f;
  static final float ROTATION_SPEED = 180.0f / Constants.Step.STEPS_PER_SECOND;

  Level level;
  Body body;
  Sprite sprite;

  private int age = 0;
  private boolean isDestroyed = false;

  public void create(final SpriteLoader spriteLoader, final Level level, final Vector2 position) {
    this.level = level;
    body = createBody(position);
    sprite = createSprite(spriteLoader);
  }

  @VisibleForTesting
  Sprite createSprite(final SpriteLoader spriteLoader) {
    final Sprite sprite = spriteLoader.createSprite(getTexturePath());
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setOriginBasedPosition(body.getPosition().x, body.getPosition().y);
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    return sprite;
  }

  @VisibleForTesting
  Body createBody(final Vector2 position) {
    final BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyDef.BodyType.StaticBody;
    bodyDef.position.set(position);
    final CircleShape circleShape = new CircleShape();
    circleShape.setRadius(RADIUS);
    final FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = circleShape;
    final Body body = level.getWorld().createBody(bodyDef);
    body.createFixture(fixtureDef);
    body.setUserData(this);
    circleShape.dispose();
    return body;
  }

  public abstract Type getType();

  public abstract void apply(final Lobster lobster);

  abstract String getTexturePath();

  Level getLevel() {
    return level;
  }

  @Override
  public Body getBody() {
    return body;
  }

  @Override
  public void destroy() {
    if (isDestroyed()) {
      return;
    }

    isDestroyed = true;
    level.destroy(this);
  }

  @Override
  public boolean isDestroyed() {
    return isDestroyed;
  }

  @Override
  public void render(final Batch batch) {
    sprite.draw(batch);
  }

  @Override
  public void contact(final LobsterContact lobsterContact, final Lobster lobster) {
    lobster.contactPowerUp(lobsterContact, this);
  }

  @Override
  public void step(
      final float timeStep, final int velocityIterations, final int positionIterations) {
    if (++age > AGE_LIMIT) {
      getLevel().destroy(this);
    }
  }

  @Override
  public void dispose() {
  }
}
