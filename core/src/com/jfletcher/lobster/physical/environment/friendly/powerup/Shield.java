package com.jfletcher.lobster.physical.environment.friendly.powerup;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.jfletcher.lobster.audio.SoundEffectManager;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.player.lobster.Lobster;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.particle.TextParticle;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;
import com.jfletcher.lobster.visual.text.TextElement;

import java.util.List;

public class Shield extends PowerUp {

  public static final Color COLOR = new Color(0xa020f7ff);
  private static final List<String> PHRASES =
      List.of(
          "Shell yes!",
          "It's shell time",
          "Defense!",
          "I got your back",
          "I'm Urchin for a fight",
          "Bring it on",
          "Urchin tough",
          "We got this",
          "Stay strong",
          "Can't touch this!");
  private static final int SHIELD_LEVEL = 1;
  private static final String TEXTURE = "textures/urchin.png";
  private static final float MAX_SIZE_FLUCTUATION = 0.1f;
  private static final double ROTATION_SCALAR = 6 * Constants.Step.TIME_STEP;

  private double angle = 0f;

  @Override
  public void create(final SpriteLoader spriteLoader, final Level level, final Vector2 position) {
    super.create(spriteLoader, level, position);
  }

  public void apply(final Lobster lobster) {
    lobster.getLevel().getSoundEffectManager().play(SoundEffectManager.SoundEffect.SHIELD);
    lobster.addShield(SHIELD_LEVEL);
    // TODO make this DRY
    final String phrase = RandomUtil.randomElement(PHRASES);
    final TextParticle textParticle = new TextParticle();
    textParticle.create(
        level,
        TextElement.Alignment.CENTER,
        body.getPosition().x,
        body.getPosition().y,
        () -> phrase,
        16,
        COLOR);
    level.addTextElement(textParticle);
  }

  @Override
  String getTexturePath() {
    return TEXTURE;
  }

  @Override
  public Type getType() {
    return Type.SHIELD;
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    sprite.setScale(
        (float)
            (Constants.Scene.WORLD_TO_SCREEN
                * (1 + MAX_SIZE_FLUCTUATION * Math.sin(angle += ROTATION_SCALAR))));
    super.step(timeStep, velocityIterations, positionIterations);
  }

  @Override
  public void render(final Batch batch) {
    super.render(batch);
  }
}
