package com.jfletcher.lobster.physical.environment.friendly.powerup;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.jfletcher.lobster.audio.SoundEffectManager;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.player.lobster.Lobster;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.particle.TextParticle;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;
import com.jfletcher.lobster.visual.text.TextElement;

import java.util.List;

public class Slow extends PowerUp {

  public static final Color COLOR = new Color(0xd46231ff);
  private static final List<String> PHRASES =
      List.of(
          "Ha!",
          "Stick 'em!",
          "Flush 'em!",
          "Slice 'em!",
          "They'll see",
          "Eat slime!",
          "They're leaving",
          "Two claws good",
          "Eight legs bad",
          "For the sea!",
          "We have to win",
          "Death to spiders!");
  private static final float SLOW_BY = 0.5f;
  private static final String TEXTURE = "textures/snail_0.png";

  @Override
  public void create(final SpriteLoader spriteLoader, final Level level, final Vector2 position) {
    super.create(spriteLoader, level, position);
    sprite.setRotation(RandomUtil.floatRange(0.0f, 360.0f));
  }

  @Override
  public Type getType() {
    return Type.SLOW;
  }

  @Override
  public void apply(final Lobster lobster) {
    lobster.getLevel().getSoundEffectManager().play(SoundEffectManager.SoundEffect.SLOW);
    lobster.slowSpiders(SLOW_BY);
    // TODO make this DRY
    final String phrase = RandomUtil.randomElement(PHRASES);
    final TextParticle textParticle = new TextParticle();
    textParticle.create(
        level,
        TextElement.Alignment.CENTER,
        body.getPosition().x,
        body.getPosition().y,
        () -> phrase,
        16,
        COLOR);
    level.addTextElement(textParticle);
  }

  @Override
  String getTexturePath() {
    return TEXTURE;
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    sprite.rotate(ROTATION_SPEED);
    super.step(timeStep, velocityIterations, positionIterations);
  }

  @Override
  public void render(final Batch batch) {
    super.render(batch);
  }
}
