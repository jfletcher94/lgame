package com.jfletcher.lobster.physical.environment.friendly;

import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.InLevel;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.google.common.annotations.VisibleForTesting;

public class Bouncer implements InLevel {

  private interface Const {

    interface Shape {
      float WIDTH = 1.125f;
      float HEIGHT = 0.25f;
    }

    interface Texture {
      String BOUNCER = "textures/bouncer.png";
    }
  }

  private Level level;
  private Sprite sprite;
  private Body body;
  private SpriteLoader spriteLoader;

  public void create(
      final Level level,
      final SpriteLoader spriteLoader,
      final Vector2 position,
      final float angle,
      final float restitution) {
    this.level = level;
    this.spriteLoader = spriteLoader;
    sprite = createSprite(position, angle);
    body = createBody(position, angle, restitution);
  }

  @VisibleForTesting
  Sprite createSprite(final Vector2 position, final float angle) {
    final Sprite sprite = spriteLoader.createSprite(Const.Texture.BOUNCER);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setSize(Const.Shape.WIDTH, Const.Shape.HEIGHT);
    sprite.setOriginCenter();
    sprite.setPosition(position.x - sprite.getOriginX(), position.y - sprite.getOriginY());
    sprite.rotate(angle);
    return sprite;
  }

  @VisibleForTesting
  Body createBody(final Vector2 position, final float angle, final float restitution) {
    final BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyDef.BodyType.StaticBody;
    bodyDef.position.set(position);
    bodyDef.angle = (float) Math.toRadians(angle);
    final FixtureDef nonBounceSide = new FixtureDef();
    final FixtureDef bounceSide = new FixtureDef();
    final PolygonShape nonBounceShape = new PolygonShape();
    nonBounceShape.setAsBox(
        Const.Shape.WIDTH * 0.5f,
        Const.Shape.HEIGHT * 0.25f,
        new Vector2(0.0f, Const.Shape.HEIGHT * -0.25f),
        0.0f);
    nonBounceSide.shape = nonBounceShape;
    final PolygonShape bounceShape = new PolygonShape();
    bounceShape.setAsBox(
        Const.Shape.WIDTH * 0.5f,
        Const.Shape.HEIGHT * 0.25f,
        new Vector2(0.0f, Const.Shape.HEIGHT * 0.25f),
        0.0f);
    bounceSide.shape = bounceShape;
    bounceSide.restitution = restitution;
    final Body body = level.getWorld().createBody(bodyDef);
    body.createFixture(nonBounceSide);
    body.createFixture(bounceSide);
    body.setUserData(this);
    nonBounceShape.dispose();
    bounceShape.dispose();
    return body;
  }

  @Override
  public Body getBody() {
    return body;
  }

  @Override
  public void destroy() {
  }

  @Override
  public boolean isDestroyed() {
    return false;
  }

  @Override
  public void render(final Batch batch) {
    sprite.draw(batch);
  }

  @Override
  public void step(
      final float timeStep, final int velocityIterations, final int positionIterations) {
  }

  @Override
  public void dispose() {
  }
}
