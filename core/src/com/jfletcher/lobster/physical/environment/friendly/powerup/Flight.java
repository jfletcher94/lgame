package com.jfletcher.lobster.physical.environment.friendly.powerup;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.jfletcher.lobster.audio.SoundEffectManager;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.player.lobster.Lobster;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.particle.TextParticle;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;
import com.jfletcher.lobster.visual.text.TextElement;

import java.util.List;

public class Flight extends PowerUp {

  private static final List<String> PHRASES =
      List.of(
          "Hello!",
          "Weeeeeeee",
          "Let's go!",
          "Time to fly",
          "On a jetpack",
          "Let's turbo!",
          "Faster, faster!",
          "Never stop!",
          "Don't stop now",
          "Can't stop us!",
          "Who's shrimpy now?!");
  private static final String TEXTURE = "textures/shrimp_0.png";

  @Override
  public void create(final SpriteLoader spriteLoader, final Level level, final Vector2 position) {
    super.create(spriteLoader, level, position);
    sprite.setRotation(RandomUtil.floatRange(0.0f, 360.0f));
  }

  @Override
  public void apply(final Lobster lobster) {
    lobster.getLevel().getSoundEffectManager().play(SoundEffectManager.SoundEffect.FLIGHT);
    lobster.addFlight();
    // TODO make this DRY
    final String phrase = RandomUtil.randomElement(PHRASES);
    final TextParticle textParticle = new TextParticle();
    textParticle.create(
        level,
        TextElement.Alignment.CENTER,
        body.getPosition().x,
        body.getPosition().y,
        () -> phrase,
        16,
        Color.PINK);
    level.addTextElement(textParticle);
  }

  @Override
  String getTexturePath() {
    return TEXTURE;
  }

  @Override
  public Type getType() {
    return Type.FLIGHT;
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    sprite.rotate(-ROTATION_SPEED);
    super.step(timeStep, velocityIterations, positionIterations);
  }

  @Override
  public void render(final Batch batch) {
    super.render(batch);
  }
}
