package com.jfletcher.lobster.physical.environment.neutral;

import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.InLevel;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.google.common.annotations.VisibleForTesting;

public class Block implements InLevel {

  private interface Const {

    interface Texture {
      String BLOCK = "textures/block.png";
    }
  }

  private Level level;
  private Sprite sprite;
  private Body body;
  private SpriteLoader spriteLoader;

  public void create(
      final Level level,
      final SpriteLoader spriteLoader,
      final Vector2 position,
      final Vector2 size) {
    this.spriteLoader = spriteLoader;
    this.level = level;
    sprite = createSprite(position, size);
    body = createBody(position, size);
  }

  @VisibleForTesting
  Sprite createSprite(final Vector2 position, final Vector2 size) {
    final Sprite sprite = spriteLoader.createSprite(Const.Texture.BLOCK);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setPosition(position.x - size.x * 0.5f, position.y - size.y * 0.5f);
    sprite.setSize(size.x, size.y);
    return sprite;
  }

  @VisibleForTesting
  Body createBody(final Vector2 position, final Vector2 size) {
    final BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyDef.BodyType.StaticBody;
    bodyDef.position.set(position);
    final PolygonShape polygonShape = new PolygonShape();
    polygonShape.setAsBox(size.x * 0.5f, size.y * 0.5f);
    final FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = polygonShape;
    final Body body = level.getWorld().createBody(bodyDef);
    body.createFixture(fixtureDef);
    body.setUserData(this);
    polygonShape.dispose();
    return body;
  }

  @Override
  public Body getBody() {
    return body;
  }

  @Override
  public void destroy() {

  }

  @Override
  public boolean isDestroyed() {
    return false;
  }

  @Override
  public void render(final Batch batch) {
    sprite.draw(batch);
  }

  @Override
  public void step(
      final float timeStep, final int velocityIterations, final int positionIterations) {
  }

  @Override
  public void dispose() {
  }
}
