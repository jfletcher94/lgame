package com.jfletcher.lobster.physical.environment.neutral;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.Body;
import com.jfletcher.lobster.physical.contact.Contactable;
import com.jfletcher.lobster.physical.contact.LobsterContact;
import com.jfletcher.lobster.physical.player.lobster.Lobster;

public class Wall implements Contactable {

  private Body body;

  public void create(final Body body) {
    this.body = body;
    body.setUserData(this);
  }

  @Override
  public void contact(final LobsterContact lobsterContact, final Lobster lobster) {
    lobster.contactWall(this);
  }

  @Override
  public Body getBody() {
    return body;
  }

  @Override
  public void destroy() {}

  @Override
  public boolean isDestroyed() {
    return false;
  }

  @Override
  public void render(final Batch batch) {}

  @Override
  public void step(
      final float timeStep, final int velocityIterations, final int positionIterations) {}

  @Override
  public void dispose() {}
}
