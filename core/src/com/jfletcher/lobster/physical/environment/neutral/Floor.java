package com.jfletcher.lobster.physical.environment.neutral;

import com.jfletcher.lobster.physical.contact.LobsterContact;
import com.jfletcher.lobster.physical.player.lobster.Lobster;

public class Floor extends Wall {

  @Override
  public void contact(final LobsterContact lobsterContact, final Lobster lobster) {
    super.contact(lobsterContact, lobster);
    lobster.contactFloor(this);
  }
}
