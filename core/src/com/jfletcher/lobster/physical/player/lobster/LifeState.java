package com.jfletcher.lobster.physical.player.lobster;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.Align;
import com.jfletcher.lobster.audio.SoundEffectManager;
import com.jfletcher.lobster.hud.PreferenceManager;
import com.jfletcher.lobster.hud.ScoreMenu;
import com.jfletcher.lobster.level.Menu;
import com.jfletcher.lobster.physical.Steppable;
import com.jfletcher.lobster.physical.environment.friendly.powerup.Shield;
import com.jfletcher.lobster.physical.nonplayer.Enemy;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.StyleManager;
import com.jfletcher.lobster.visual.particle.BodyPartParticle;
import com.jfletcher.lobster.visual.particle.Bubble;
import com.jfletcher.lobster.visual.particle.DebrisParticle;
import org.apache.commons.lang3.tuple.Pair;

class LifeState implements Steppable {

  private static final int INVINCIBILITY_FRAMES = 1 * Constants.Step.STEPS_PER_SECOND;
  private static final Label.LabelStyle labelStyle =
      StyleManager.createLabelStyle(24, Color.BLACK, 2, Color.WHITE);
  private static final TextField.TextFieldStyle textFieldStyle =
      StyleManager.createTextFieldStyle(24, Color.RED, 2, Color.WHITE);

  private final Lobster lobster;

  private boolean isDead = false;
  private int shieldLevel = 0;
  private int invincibility = 0;

  LifeState(final Lobster lobster) {
    this.lobster = lobster;
  }

  void die() {
    if (isDead()) {
      return;
    }

    lobster.getLevel().getSoundEffectManager().play(SoundEffectManager.SoundEffect.DIE);

    final var bodyPartParticles =
        BodyPartParticle.create(
            lobster.getLevel().getSpriteLoader(),
            lobster.getLevel(),
            lobster.getBody().getPosition(),
            lobster.getBody().getLinearVelocity());
    bodyPartParticles.forEach(lobster.getLevel()::addVisualElementFront);

    final int newBestKill =
        PreferenceManager.putBestKill(lobster.getHighestScoreIncrement(), "Player");
    final int newScore = PreferenceManager.putHighScore(lobster.getScore(), "Player");
    final int newKills = PreferenceManager.putKillCount(lobster.getKills(), "Player");
    if (newBestKill >= 0 || newScore >= 0 || newKills >= 0) {
      lobster.getLevel().getInputManager().setStage(lobster.getLevel().getStage());
      final TextField textField = new TextField("Player", textFieldStyle);
      textField.setCursorPosition(textField.getText().length());
      textField.setMaxLength(16);
      textField.setAlignment(Align.center);
      textField.setTextFieldFilter((t, c) -> Character.isLetter(c));
      final Table table = new Table();
      table.add(new Label("New High Score!", labelStyle)).align(Align.center).padBottom(12f);
      table.row().padBottom(12f);
      table.add(textField).width(Constants.Scene.UI_SIZE.x).align(Align.center).padBottom(12f);
      table.row().padBottom(12f);
      table.setPosition(Constants.Scene.UI_HALF_SIZE.x, Constants.Scene.UI_HALF_SIZE.y);
      final Menu menu =
          new Menu(
              lobster.getLevel().getInputManager(),
              lobster.getLevel().getSoundEffectManager(),
              table,
              () -> {},
              labelStyle,
              12f,
              Pair.of(
                  "Done",
                  () -> {
                    final String text = textField.getText().strip();
                    if (text.length() > 0) {
                      if (newBestKill >= 0) PreferenceManager.renameBestKill(newBestKill, text);
                      if (newScore >= 0) PreferenceManager.renameHighScore(newScore, text);
                      if (newKills >= 0) PreferenceManager.renameKillCount(newKills, text);
                    }
                    table.remove();
                    lobster.getLevel().getInputManager().setStage(null);
                    setScoreMenu(newBestKill, newScore, newKills);
                  }));
      lobster.getLevel().getStage().addActor(table);
      lobster.getLevel().getStage().setKeyboardFocus(textField);
      lobster.getLevel().setMenu(menu);
    } else {
      setScoreMenu(newBestKill, newScore, newKills);
    }

    isDead = true;
    lobster.getLevel().destroy(lobster);
  }

  private void setScoreMenu(int newBestKill, int newScore, int newKills) {
    final ScoreMenu scoreMenu = new ScoreMenu();
    scoreMenu.setNew(newBestKill, newScore, newKills);
    scoreMenu
        .getTable()
        .setPosition(Constants.Scene.UI_HALF_SIZE.x, Constants.Scene.UI_HALF_SIZE.y, Align.center);
    final Menu menu =
        new Menu(
            lobster.getLevel().getInputManager(),
            lobster.getLevel().getSoundEffectManager(),
            scoreMenu.getTable(),
            Gdx.app::exit,
            labelStyle,
            12f,
            Pair.of(
                "Play Again",
                () -> {
                  lobster
                      .getLevel()
                      .getSoundEffectManager()
                      .play(SoundEffectManager.SoundEffect.START);
                  lobster.getLevel().restart();
                }),
            Pair.of("Quit", Gdx.app::exit));
    lobster.getLevel().getStage().addActor(scoreMenu.getTable());
    lobster.getLevel().setMenu(menu);
  }

  boolean isDead() {
    return isDead;
  }

  void contactEnemy(final Enemy enemy) {
    if (invincibility > 0) {
      return;
    }

    lobster.getLevel().getSoundEffectManager().play(SoundEffectManager.SoundEffect.HIT);
    invincibility = INVINCIBILITY_FRAMES;
    if ((shieldLevel -= enemy.getDamage()) < 0) {
      die();
      return;
    }

    // FIXME magic numbers
    final int numBubbles = RandomUtil.intRange(4, 6);
    for (int i = 0; i < numBubbles; i++) {
      final Bubble bubble = new Bubble();
      bubble.create(
          lobster.getLevel().getSpriteLoader(),
          lobster.getLevel(),
          lobster.getBody().getPosition().x + RandomUtil.floatRange(-0.5f, 0.5f),
          lobster.getBody().getPosition().y + RandomUtil.floatRange(-0.5f, 0.5f),
          true);
      lobster.getLevel().addVisualElementFront(bubble);
    }
    for (int i = 0; i < 8; i++) {
      final DebrisParticle debrisParticle = new DebrisParticle();
      debrisParticle.create(
          lobster.getLevel().getSpriteLoader(),
          lobster.getLevel(),
          lobster.getBody().getPosition().x,
          lobster.getBody().getPosition().y,
          Shield.COLOR);
      lobster.getLevel().addVisualElementFront(debrisParticle);
    }
  }

  void addShield(final int shieldLevel) {
    this.shieldLevel += shieldLevel;
  }

  int getShieldLevel() {
    return shieldLevel;
  }

  boolean isInvincible() {
    return invincibility > 0;
  }

  @Override
  public void step(float timeStep, int velocityIterations, int positionIterations) {
    if (invincibility > 0) {
      invincibility--;
    }
  }
}
