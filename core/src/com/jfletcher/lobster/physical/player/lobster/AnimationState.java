package com.jfletcher.lobster.physical.player.lobster;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

class AnimationState {

  private interface Const {

    interface Body {
      String LOBSTER = "textures/lobster1_a.png";
      String SHIELD = "textures/shield.png";
      int REGION_WIDTH = 64;
      int REGION_HEIGHT = 64;
      int REGION_COUNT = 8;
      int SPRITE_ANIMATION_SCALE = 4;
    }

    interface Claw {
      String CLAWS = "textures/claw_a.png";
      int REGION_WIDTH = 64;
      int REGION_HEIGHT = 64;
      int SPRITE_ANIMATION_SCALE = Constants.Step.STEPS_PER_SECOND / 3;
    }
  }

  private final Lobster lobster;
  private final Sprite lobsterSprite;
  private final Sprite clawsSprite;
  private final Sprite shieldSprite;

  private int spriteRegion = 0;
  private boolean shieldEnabled;
  private int clawSnapFrames = -1;

  AnimationState(Lobster lobster, final SpriteLoader spriteLoader) {
    this.lobster = lobster;
    lobsterSprite = createLobsterSprite(spriteLoader);
    clawsSprite = createClawsSprite(spriteLoader);
    shieldSprite = createShieldSprite(spriteLoader);
  }

  private Sprite createLobsterSprite(final SpriteLoader spriteLoader) {
    final Sprite sprite = spriteLoader.createSprite(Const.Body.LOBSTER);
    sprite.setRegion(0, 0, Const.Body.REGION_WIDTH, Const.Body.REGION_HEIGHT);
    sprite.setSize(Const.Body.REGION_WIDTH, Const.Body.REGION_HEIGHT);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    return sprite;
  }

  private Sprite createClawsSprite(final SpriteLoader spriteLoader) {
    final Sprite sprite = spriteLoader.createSprite(Const.Claw.CLAWS);
    sprite.setRegion(0, 0, Const.Claw.REGION_WIDTH, Const.Claw.REGION_HEIGHT);
    sprite.setSize(Const.Claw.REGION_WIDTH, Const.Claw.REGION_HEIGHT);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    return sprite;
  }

  private Sprite createShieldSprite(final SpriteLoader spriteLoader) {
    final Sprite sprite = spriteLoader.createSprite(Const.Body.SHIELD);
    sprite.getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    sprite.setOriginCenter();
    // make image smaller to remove extra magic number
    sprite.setScale(Constants.Scene.WORLD_TO_SCREEN);
    return sprite;
  }

  void cycleSpriteRegion() {
    if (lobster.getLevel().getSteps() % Const.Body.SPRITE_ANIMATION_SCALE != 0) {
      return;
    }

    if (++spriteRegion >= Const.Body.REGION_COUNT) {
      spriteRegion = 0;
    }
    lobsterSprite.setRegion(
        Const.Body.REGION_WIDTH * spriteRegion,
        0,
        Const.Body.REGION_WIDTH,
        Const.Body.REGION_HEIGHT);
  }

  void snapClaws() {
    clawSnapFrames = Const.Claw.SPRITE_ANIMATION_SCALE;
    clawsSprite.setRegion(
        Const.Claw.REGION_WIDTH, 0, Const.Claw.REGION_WIDTH, Const.Claw.REGION_HEIGHT);
  }

  void move(final MovementState movementState) {
    lobsterSprite.setOriginBasedPosition(
            movementState.getBody().getPosition().x, movementState.getBody().getPosition().y);
    // TODO magic number
    lobsterSprite.setRotation(movementState.getBody().getLinearVelocity().angle() - 90.0f);
    clawsSprite.setOriginBasedPosition(
            movementState.getBody().getPosition().x, movementState.getBody().getPosition().y);
    // TODO magic number
    clawsSprite.setRotation(movementState.getBody().getLinearVelocity().angle() - 90.0f);
    shieldSprite.setOriginBasedPosition(
        movementState.getBody().getPosition().x, movementState.getBody().getPosition().y);
    shieldSprite.setRotation(movementState.getBody().getLinearVelocity().angle());
    if (clawSnapFrames == 0) {
      clawSnapFrames--;
      clawsSprite.setRegion(0, 0, Const.Claw.REGION_WIDTH, Const.Claw.REGION_HEIGHT);
    } else if (clawSnapFrames > 0) {
      clawSnapFrames--;
    }
  }

  void draw(final Batch batch) {
    if (lobster.isInvincible() && lobster.getLevel().getSteps() % 6 < 3) {
      return;
    }

    lobsterSprite.draw(batch);
    clawsSprite.draw(batch);
    if (shieldEnabled) {
      shieldSprite.draw(batch);
    }
  }

  void setShieldEnabled(final boolean shieldEnabled) {
    if (this.shieldEnabled == shieldEnabled) {
      return;
    }

    this.shieldEnabled = shieldEnabled;
  }
}
