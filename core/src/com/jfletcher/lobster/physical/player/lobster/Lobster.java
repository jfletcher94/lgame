package com.jfletcher.lobster.physical.player.lobster;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.audio.SoundEffectManager;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.InLevel;
import com.jfletcher.lobster.physical.contact.LobsterContact;
import com.jfletcher.lobster.physical.environment.friendly.powerup.PowerUp;
import com.jfletcher.lobster.physical.environment.neutral.Floor;
import com.jfletcher.lobster.physical.environment.neutral.Wall;
import com.jfletcher.lobster.physical.nonplayer.hermitcrab.HermitCrab;
import com.jfletcher.lobster.physical.nonplayer.hermitcrab.SharpShell;
import com.jfletcher.lobster.physical.nonplayer.spider.Spider;
import com.jfletcher.lobster.physical.nonplayer.spider.Web;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.Logger;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.particle.Bubble;
import com.jfletcher.lobster.visual.particle.TrailParticle;
import com.jfletcher.lobster.visual.sprite.SpriteLoader;

import java.util.ArrayList;
import java.util.List;

public class Lobster implements InLevel {

  @VisibleForTesting static final Logger logger = new Logger(Lobster.class, Logger.Level.DEBUG);

  private Level level;
  private SpriteLoader spriteLoader;

  private MovementState movementState;
  private LifeState lifeState;
  private AnimationState animationState;
  private List<Runnable> stepActions;
  private Score score = new Score();

  private int kills = 0;
  private boolean shieldChanged = false;
  private boolean contactThisFrame = false;

  public void create(
      final SpriteLoader spriteLoader,
      final Level level,
      final Vector2 position,
      final Vector2 velocity) {
    this.level = level;
    this.spriteLoader = spriteLoader;
    lifeState = createLifeState();
    animationState = createAnimationState();
    movementState = createMovementState(position, velocity);
    stepActions = new ArrayList<>();
  }

  @VisibleForTesting
  LifeState createLifeState() {
    return new LifeState(this);
  }

  @VisibleForTesting
  AnimationState createAnimationState() {
    return new AnimationState(this, spriteLoader);
  }

  @VisibleForTesting
  MovementState createMovementState(final Vector2 position, final Vector2 velocity) {
    final MovementState movementState = new MovementState(position, velocity, level);
    movementState.getBody().setUserData(this);
    return movementState;
  }

  @Override
  public void step(
      final float timeStep, final int velocityIterations, final int positionIterations) {
    if (lifeState.isDead()) {
      return;
    }

    contactThisFrame = false;
    animationState.cycleSpriteRegion();
    animationState.move(movementState);
    lifeState.step(timeStep, velocityIterations, positionIterations);
    TrailParticle.createParticles(spriteLoader, level, getBody(), movementState.isFlying(), false);
    stepActions.forEach(Runnable::run);
    stepActions.clear();
    if (shieldChanged) {
      shieldChanged = false;
      animationState.setShieldEnabled(lifeState.getShieldLevel() > 0);
    }
    movementState.move();
    movementState.checkForDeath(lifeState);
  }

  @Override
  public Body getBody() {
    return movementState.getBody();
  }

  @Override
  public void destroy() {
    lifeState.die();
  }

  @Override
  public boolean isDestroyed() {
    return lifeState.isDead();
  }

  @Override
  public void render(final Batch batch) {
    if (lifeState.isDead()) {
      return;
    }

    animationState.draw(batch);
  }

  @Override
  public void dispose() {}

  public long getScore() {
    return score.getScore();
  }

  public long getHighestScoreIncrement() {
    return score.getHighestScoreIncrement();
  }

  public void contactSpider(final LobsterContact lobsterContact, final Spider spider) {
    lobsterContact.getContact().setEnabled(false);
    if (contactThisFrame || isInvincible() || spider.isDestroyed()) {
      return;
    }

    contactThisFrame = true;
    stepActions.add(
        () -> {
          shieldChanged = true;
          lifeState.contactEnemy(spider);
          score.reset();
        });
  }

  public void contactWeb(final LobsterContact lobsterContact, final Web web) {
    lobsterContact.getContact().setEnabled(false);
    if (contactThisFrame || web.isDestroyed()) {
      return;
    }

    contactThisFrame = true;
    stepActions.add(
        () -> {
          web.destroy();
          movementState.contactWeb(web);
          animationState.snapClaws();
          kills++;
          score.killSpider(kills, movementState.isFlying());
          score.addKillToScore(getBody().getPosition().dst(web.getSpider().getPosition()), kills);
          addKillSpiderVisuals(web.getSpider());
        });
  }

  public void contactHermitCrab(final LobsterContact lobsterContact, final HermitCrab hermitCrab) {
    if (contactThisFrame || hermitCrab.isDestroyed()) {
      return;
    }

    contactThisFrame = true;
    lobsterContact.getContact().setEnabled(false);
    getLevel().getSoundEffectManager().play(SoundEffectManager.SoundEffect.SNIP2);
    movementState.bounce(false, true);
    score.killHermitCrab();
    hermitCrab.addDeathVisuals();
    hermitCrab.destroy();
  }

  public void contactSharpShell(final LobsterContact lobsterContact, final SharpShell sharpShell) {
    if (contactThisFrame || isInvincible() || sharpShell.isDestroyed()) {
      return;
    }

    contactThisFrame = true;
    shieldChanged = true;
    lifeState.contactEnemy(sharpShell);
    score.reset();
  }

  public void contactPowerUp(final LobsterContact lobsterContact, final PowerUp powerUp) {
    lobsterContact.getContact().setEnabled(false);
    powerUp.apply(this);
    level.destroy(powerUp);
  }

  public void contactFloor(final Floor floor) {
    score.reset();
    addFloorContactVisuals(floor);
  }

  public void contactWall(final Wall wall) {
    level.getSoundEffectManager().play(SoundEffectManager.SoundEffect.BOUNCE);
  }

  public void addShield(final int shieldLevel) {
    shieldChanged = true;
    lifeState.addShield(shieldLevel);
  }

  public void addFlight() {
    movementState.addFlight();
  }

  public void slowSpiders(final float scalar) {
    getLevel().getSpiders().forEach(spider -> spider.slow(scalar));
  }

  public Level getLevel() {
    return level;
  }

  public int getKills() {
    return kills;
  }

  private void addFloorContactVisuals(final Floor floor) {
    // FIXME magic numbers
    final float ySpeed = Math.abs(getBody().getLinearVelocity().y);
    final int numBubbles;
    if (ySpeed > 15) {
      numBubbles = 3 + RandomUtil.intRange(0, 2);
    } else if (ySpeed > 10) {
      numBubbles = 2 + RandomUtil.intRange(0, 2);
    } else if (ySpeed > 5) {
      numBubbles = 1 + RandomUtil.intRange(0, 2);
    } else {
      numBubbles = 1;
    }
    for (int i = 0; i < numBubbles; i++) {
      final Bubble bubble = new Bubble();
      bubble.create(
          spriteLoader,
          level,
          getBody().getPosition().x + RandomUtil.floatRange(-0.5f, 0.5f),
          -Constants.Scene.HALF_SIZE.y
              - Constants.Scene.WORLD_TO_SCREEN * Bubble.HALF_SPRITE_HEIGHT,
          true);
      level.addVisualElementFront(bubble);
    }
  }

  private void addKillSpiderVisuals(final Spider spider) {
    spider.addDeathVisuals();
    score.createScoreParticle(level, spider.getPosition().x, getBody().getPosition().y);
  }

  boolean isInvincible() {
    return lifeState.isInvincible();
  }

  public boolean isDead() {
    return lifeState.isDead();
  }
}
