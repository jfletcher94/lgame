package com.jfletcher.lobster.physical.player.lobster;

import com.badlogic.gdx.graphics.Color;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.util.RandomUtil;
import com.jfletcher.lobster.visual.particle.TextParticle;
import com.jfletcher.lobster.visual.text.TextElement;

import java.text.DecimalFormat;

public class Score {

  private static final double STARTING_VALUE = 100.0;
  private static final float SCORE_BASE = 2.0f;
  private static final float SCORE_SCALAR = 1.15f;
  private static final float DIVIDE_BY_ZERO_FENCE = 0.5f;

  public static final DecimalFormat decimalFormat = new DecimalFormat("#,###");

  private long score = 0L;
  private double combo = STARTING_VALUE;
  private long highestScoreIncrement = 0L;

  private long latestScoreIncrement = 0L;
  private Color currentColor;
  private int colorPhase;
  private boolean inCombo = true;

  public Score() {
    setNextRandomColor();
  }

  public long getScore() {
    return score;
  }

  public double getCombo() {
    return combo;
  }

  public long getHighestScoreIncrement() {
    return highestScoreIncrement;
  }

  public long getLatestScoreIncrement() {
    return latestScoreIncrement;
  }

  public void reset() {
    if (!inCombo) {
      return;
    }

    inCombo = false;
    combo = 1.0;
    setNextRandomColor();
  }

  public void killSpider(final int kills, final boolean isFlying) {
    inCombo = true;
    combo += (kills * kills) / (isFlying ? 2f : 1f);
  }

  public void killHermitCrab() {
    inCombo = true;
    combo *= 2;
  }

  public void addKillToScore(final float distance, final int kills) {
    inCombo = true;
    latestScoreIncrement =
        Math.max(1, (long) (combo * (SCORE_BASE / (DIVIDE_BY_ZERO_FENCE + distance))));
    if (latestScoreIncrement > highestScoreIncrement) {
      highestScoreIncrement = latestScoreIncrement;
    }
    score += latestScoreIncrement;
    combo = (SCORE_SCALAR * combo) + kills;
  }

  public void createScoreParticle(final Level level, final float x, final float y) {
    final long scoreIncrement = latestScoreIncrement;
    final TextParticle textParticle = new TextParticle();
    textParticle.create(
        level,
        TextElement.Alignment.CENTER,
        x,
        y,
        () -> String.format("%s!", decimalFormat.format(scoreIncrement)),
        16,
        currentColor,
        currentColor.r + currentColor.g + currentColor.b > 2f ? Color.BLACK : Color.WHITE);
    textParticle.getActor().scaleBy(getScale(scoreIncrement));
    level.addTextElement(textParticle);
  }

  private void setNextRandomColor() {
    float main = RandomUtil.floatRange(0.75f, 0.95f);
    float minor1 = RandomUtil.floatRange(0.1f, 0.35f);
    float minor2 = RandomUtil.floatRange(0.1f, 0.35f);

    colorPhase += colorPhase = RandomUtil.intRange(1, 3);
    if (colorPhase >= 3) {
      colorPhase -= 3;
    }
    switch (colorPhase) {
      case 0:
        currentColor = new Color(main, minor1, minor2, 1f);
        break;
      case 1:
        currentColor = new Color(minor1, main, minor2, 1f);
        break;
      default:
        currentColor = new Color(minor1, minor2, main, 1f);
        break;
    }
  }

  private float getScale(final long scoreIncrement) {
    return Math.min(5f, (float) Math.log10(scoreIncrement) / 10f);
  }
}
