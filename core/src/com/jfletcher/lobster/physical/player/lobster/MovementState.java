package com.jfletcher.lobster.physical.player.lobster;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.google.common.annotations.VisibleForTesting;
import com.jfletcher.lobster.audio.SoundEffectManager;
import com.jfletcher.lobster.level.Level;
import com.jfletcher.lobster.physical.nonplayer.spider.Web;
import com.jfletcher.lobster.util.Constants;
import com.jfletcher.lobster.util.InputManager;

class MovementState {

  // Body constants
  private static final float RADIUS = 0.15f;
  private static final float DENSITY = 1.0f;
  private static final float FRICTION = 0.0f;
  private static final float RESTITUTION = 0.95f;
  private static final float MASS = (float) (Math.PI * RADIUS * RADIUS * DENSITY);

  // Movement constants
  private static final int FLIGHT_INCREMENT = 10 * Constants.Step.STEPS_PER_SECOND;
  private static final float FLIGHT_START_SCALAR = 0.25f;
  private static final float RESISTANCE_SCALAR = 0.0005f;
  private static final float MAX_SPEED_X = 8.0f;
  private static final float MAX_SPEED_FLIGHT = (float) Math.sqrt(MAX_SPEED_X * MAX_SPEED_X * 2);
  private static final float INPUT_FORCE = 25.0f * MASS;
  private static final Vector2 HORIZONTAL_FORCE = new Vector2(INPUT_FORCE, 0.0f);
  private static final Vector2 VERTICAL_FORCE = new Vector2(0.0f, INPUT_FORCE);
  private static final Vector2 GRAVITY = Level.Const.STANDARD_GRAVITY.cpy().scl(MASS);

  // Movement death constants
  private static final float DEATH_HEIGHT_THRESHOLD = Constants.Scene.HALF_SIZE.y * -0.92f;
  private static final float DEATH_SPEED_THRESHOLD = 0.04f;

  // Web bounce constants
  private static final float HIGH_SPEED_THRESHOLD = 4.0f;
  private static final float HIGH_SPEED_BOOST = 2.0f;
  private static final float HIGH_NEGATIVE_SPEED_THRESHOLD =
      -HIGH_SPEED_THRESHOLD - HIGH_SPEED_BOOST;

  private final Level level;
  private final Body body;

  private float accelerationScalar = 1.0f;
  private float velocityScalar = 1.0f;
  private int flightCounter = 0;

  public MovementState(final Vector2 position, final Vector2 velocity, final Level level) {
    this.level = level;
    final BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyDef.BodyType.DynamicBody;
    bodyDef.position.set(position);
    bodyDef.linearVelocity.set(velocity);
    final CircleShape circleShape = new CircleShape();
    circleShape.setRadius(RADIUS);
    final FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = circleShape;
    fixtureDef.density = DENSITY;
    fixtureDef.friction = FRICTION;
    fixtureDef.restitution = RESTITUTION;
    body = level.getWorld().createBody(bodyDef);
    body.createFixture(fixtureDef);
    circleShape.dispose();
  }

  Body getBody() {
    return body;
  }

  void move() {
    applyUserInput();
    applyForces();
    limitSpeed();
    if (isFlying()) {
      flightCounter--;
    }
  }

  @VisibleForTesting
  void applyUserInput() {
    body.applyForceToCenter(
        HORIZONTAL_FORCE
            .cpy()
            .scl(
                accelerationScalar * level.getInputManager().getAxis(InputManager.Axis.HORIZONTAL)),
        true);
    if (!isFlying()) {
      return;
    }

    body.applyForceToCenter(
        VERTICAL_FORCE
            .cpy()
            .scl(accelerationScalar * level.getInputManager().getAxis(InputManager.Axis.VERTICAL)),
        true);
  }

  @VisibleForTesting
  void applyForces() {
    if (!isFlying()) {
      body.applyForceToCenter(GRAVITY, true);
    }
    body.applyForceToCenter(
        new Vector2(RESISTANCE_SCALAR * body.getLinearVelocity().len2(), 0.0f)
            .setAngle(-body.getLinearVelocity().angle()),
        true);
  }

  @VisibleForTesting
  void limitSpeed() {
    final Vector2 linearVelocity = body.getLinearVelocity();
    final float linearSpeed = linearVelocity.len();
    if (isFlying()) {
      if (linearSpeed > MAX_SPEED_FLIGHT) {
        body.setLinearVelocity(linearVelocity.scl(MAX_SPEED_FLIGHT / linearSpeed));
      }
      return;
    }

    final float scaledMaxSpeedX = velocityScalar * MAX_SPEED_X;
    if (linearVelocity.x > scaledMaxSpeedX) {
      body.setLinearVelocity(scaledMaxSpeedX, linearVelocity.y);
    } else if (linearVelocity.x < -scaledMaxSpeedX) {
      body.setLinearVelocity(-scaledMaxSpeedX, linearVelocity.y);
    }
  }

  void checkForDeath(final LifeState lifeState) {
    if (isFlying()) {
      return;
    }

    if (body.getPosition().y < DEATH_HEIGHT_THRESHOLD
        && body.getLinearVelocity().y > 0.0f
        && body.getLinearVelocity().y < DEATH_SPEED_THRESHOLD) {
      lifeState.die();
    }
  }

  void contactWeb(final Web web) {
    level.getSoundEffectManager().play(SoundEffectManager.SoundEffect.SNIP1);
    if (isFlying()) {
      return;
    }

    if (body.getLinearVelocity().y > HIGH_SPEED_THRESHOLD) {
      // high positive speed
      body.setLinearVelocity(
          body.getLinearVelocity().x, body.getLinearVelocity().y + HIGH_SPEED_BOOST);
    } else if (body.getLinearVelocity().y > HIGH_NEGATIVE_SPEED_THRESHOLD) {
      // middle speed
      body.setLinearVelocity(body.getLinearVelocity().x, HIGH_SPEED_THRESHOLD + HIGH_SPEED_BOOST);
    } else {
      // high negative speed
      body.setLinearVelocity(body.getLinearVelocity().x, -body.getLinearVelocity().y);
    }
  }

  void addFlight() {
    if (!isFlying()) {
      body.setLinearVelocity(body.getLinearVelocity().scl(FLIGHT_START_SCALAR));
    }
    flightCounter += FLIGHT_INCREMENT - flightCounter / 2;
  }

  boolean isFlying() {
    return flightCounter > 0;
  }

  public void bounce(final boolean x, final boolean y) {
    if (x) {
      if (y) {
        body.setLinearVelocity(body.getLinearVelocity().scl(-1.0f));
      } else {
        body.setLinearVelocity(-body.getLinearVelocity().x, body.getLinearVelocity().y);
      }
      return;
    }

    if (y) {
      body.setLinearVelocity(body.getLinearVelocity().x, -body.getLinearVelocity().y);
    }
  }
}
