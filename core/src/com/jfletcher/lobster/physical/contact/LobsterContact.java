package com.jfletcher.lobster.physical.contact;

import com.jfletcher.lobster.physical.player.lobster.Lobster;
import com.jfletcher.lobster.util.Logger;
import com.badlogic.gdx.physics.box2d.Contact;

public class LobsterContact implements LContact {

  private static final Logger logger = new Logger(LobsterContact.class, Logger.Level.INFO);

  private final Contact contact;
  private final Lobster lobster;
  private final Contactable contactable;

  protected LobsterContact(final Contact contact, final Lobster lobster, final Contactable contactable) {
    this.contact = contact;
    this.lobster = lobster;
    this.contactable = contactable;
  }

  public Contact getContact() {
    return contact;
  }

  public Lobster getLobster() {
    return lobster;
  }

  public Contactable getContactable() {
    return contactable;
  }

  @Override
  public void apply() {
    contactable.contact(this, lobster);
  }
}
