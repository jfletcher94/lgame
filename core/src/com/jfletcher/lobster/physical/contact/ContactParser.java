package com.jfletcher.lobster.physical.contact;

import com.badlogic.gdx.physics.box2d.Contact;
import com.jfletcher.lobster.physical.player.lobster.Lobster;

import javax.annotation.Nullable;

public class ContactParser {

  @Nullable
  public static LContact parse(final Contact contact) {
    final Object objectA = contact.getFixtureA().getBody().getUserData();
    final Object objectB = contact.getFixtureB().getBody().getUserData();
    if (objectA instanceof Lobster) {
      if (!(objectB instanceof Contactable)) {
        return null;
      }
      return new LobsterContact(contact, (Lobster) objectA, (Contactable) objectB);
    }

    if (objectB instanceof Lobster) {
      if (!(objectA instanceof Contactable)) {
        return null;
      }
      return new LobsterContact(contact, (Lobster) objectB, (Contactable) objectA);
    }

    return null;
  }
}
