package com.jfletcher.lobster.physical.contact;

import com.jfletcher.lobster.physical.InLevel;
import com.jfletcher.lobster.physical.player.lobster.Lobster;

public interface Contactable extends InLevel {

  void contact(final LobsterContact lobsterContact, Lobster lobster);
}
