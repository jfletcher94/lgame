package com.jfletcher.lobster.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class MusicManager {

  private static MusicManager INSTANCE;

  public static MusicManager getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new MusicManager();
      INSTANCE.play();
    }
    return INSTANCE;
  }

  private static final String SURF_SHIMMY = "audio/music/surfshimmy.wav";

  private final Music music;

  private MusicManager() {
    music = Gdx.audio.newMusic(Gdx.files.internal(SURF_SHIMMY));
    music.setVolume(0.5f);
  }

  public void play() {
    if (music.isPlaying()) {
      return;
    }
    music.setLooping(true);
    music.play();
  }

  public void stop() {
    music.stop();
  }

  public void toggle() {
    if (music.isPlaying()) {
      stop();
    } else {
      play();
    }
  }
}
