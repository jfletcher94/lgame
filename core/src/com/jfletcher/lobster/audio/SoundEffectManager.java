package com.jfletcher.lobster.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class SoundEffectManager {

  public enum SoundEffect {
    BOUNCE(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/bounce.wav"))),
    DIE(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/die.wav"))),
    FLIGHT(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/flight.wav"))),
    HIT(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/hit.wav"))),
    LAUNCH(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/launch.wav"))),
    PAUSE(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/pause.wav"))),
    SELECT(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/select.wav"))),
    SHIELD(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/shield.wav"))),
    SLOW(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/slow.wav"))),
    SNIP1(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/snip.wav"))),
    SNIP2(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/snip2.wav"))),
    START(Gdx.audio.newSound(Gdx.files.internal("audio/sounds/start.wav"))),
    ;

    protected Sound sound;

    SoundEffect(final Sound sound) {
      this.sound = sound;
    }
  }

  public void play(final SoundEffect soundEffect) {
    soundEffect.sound.play();
  }

  public void stop(final SoundEffect soundEffect) {
    soundEffect.sound.stop();
  }
}
