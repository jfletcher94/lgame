package com.jfletcher.lobster.desktop;

import com.jfletcher.lobster.LGame;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
  public static void main(final String[] arg) {
    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    config.fullscreen = false;
    config.vSyncEnabled = true;
    config.width = 3200;
    config.height = 1800;
    new LwjglApplication(new LGame(), config);
  }
}
